#![allow(dead_code)]

use nom::{
    branch::alt,
    bytes::complete::{
        escaped_transform, is_not, tag, take_till, take_till1, take_until, take_until1,
    },
    character::complete::{
        i32 as text_i32, line_ending, none_of, one_of, space0, space1, u32 as text_u32,
    },
    combinator::{opt, success, value},
    multi::{many0, many1},
    number::complete::float,
    sequence::{delimited, tuple},
    Finish, IResult,
};
use std::convert::TryInto;
use std::collections::HashSet;
use serde::{Serialize, Deserialize};
use crate::symbol::{Terminal, PinType};
use crate::symbol as future;
use crate::{post_to_user, UserMessage, storage::validate_string};
use nalgebra as na;

use nom_locate::LocatedSpan;
type Span<'a> = LocatedSpan<&'a str>;

/// Converts a value in thousandths of an inch to nanometers.
///
/// This is the base unit used throughout the program.
/// To prevent e.g. mouse grids from having to also be specified
/// in thou (because that would be a HUGE PAIN and make no mistake)
/// we actually lie and mis-convert these to a convenient
/// nearby value (pretending 1 in = 25mm FLAT).
pub fn thou_to_nm(mil: i32) -> i64 {
    // 1 in = 25.4mm
    // 1 thou = 25.4 um
    // 1 thou = 25400nm
    // (mil as i64) * 25400
    // whoops my hand slipped to ensure I never
    // have to see the value 25.4 ever again
    (mil as i64) * 25000
}

// ELEMENTS
//
#[derive(Clone, Copy, Debug, Deserialize, Serialize, PartialEq)]
pub enum VerticalAlignment {
    Top,
    Center,
    Bottom,
}
#[derive(Clone, Copy, Debug, Deserialize, Serialize, PartialEq)]
pub enum HorizontalAlignment {
    Right,
    Center,
    Left,
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct Field {
    name: String,
    contents: String,
    x: i64,
    y: i64,
    size: f32,
    horizontal: bool,
    visible: bool,
    italic: bool,
    bold: bool,
    alignment: (HorizontalAlignment, VerticalAlignment),
}

impl Field {
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn contents(&self) -> &str {
        &self.contents
    }
    pub fn coords(&self) -> (i64, i64) {
        (self.x, self.y)
    }
    pub fn size(&self) -> f32 {
        self.size
    }
    pub fn visible(&self) -> bool {
        self.visible
    }
    pub fn alignment(&self) -> (HorizontalAlignment, VerticalAlignment) {
        self.alignment
    }
    /// Returns bold, italic flags as tuple
    pub fn format_flags(&self) -> (bool, bool) {
        (self.bold, self.italic)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub enum PinOrientation {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct PinGeom {
    shape: PinShape,
    invisible: bool,
}

impl PinGeom {
    pub fn visible(&self) -> bool {
        !self.invisible
    }
    pub fn shape(&self) -> &PinShape {
        &self.shape
    }
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub enum PinShape {
    Line,
    Inverted,
    Clock,
    LowActive,
    OutputLow,
    Invisible,
    Other(char),
}


#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub enum GeometryElement {
    Rect(i64, i64, i64, i64),
    Circle(i64, i64, i64),
    Arc(i64, i64, i64, f32, f32),
    Polygon(Vec<(i64, i64)>),
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub enum FillType {
    Background,
    Pen,
    Unfilled,
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub enum DrawingElement {
    Pin {
        name: String,
        x: i64,
        y: i64,
        overbar: bool,
        number: Option<String>, // Yeahhhhhhh
        length: i64,
        numsize: f32,
        namesize: f32,
        orientation: PinOrientation,
        typ: PinType,
        shape: PinGeom,
        shape_index: Option<u32>,
        alt_index: Option<u32>,
    },
    Geom {
        geom: GeometryElement,
        thickness: i64,
        shape_index: Option<u32>,
        alt_index: Option<u32>,
        fill: FillType,
    },
    Unimplemented(String),
}

/// Represents an EESCHEMA format symbol.
#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct Symbol {
    name: String,
    aliases: Vec<String>,
    fields: Vec<Field>,
    prefix: Option<String>,
    footprints: Vec<String>,
    /// offset of pin names
    pin_name_offset: i64,
    show_numbers: bool,
    show_names: bool,
    parts: u32,
    locked: bool,
    power: bool,
    drawing: Vec<DrawingElement>,
}

impl Symbol {
    fn new(
        name: String,
        aliases: Vec<String>,
        fields: Vec<Field>,
        prefix: Option<String>,
        footprints: Vec<String>,
        pin_name_offset: i64,
        show_numbers: bool,
        show_names: bool,
        parts: u32,
        locked: bool,
        power: bool,
        drawing: Vec<DrawingElement>
        ) -> Self {
            Symbol {
                name, aliases, fields, prefix, footprints, pin_name_offset,
                show_numbers, show_names, parts, locked, power, drawing,
            }
    }
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn drawing(&self) -> &Vec<DrawingElement> {
        &self.drawing
    }
    pub fn fields(&self) -> &Vec<Field> {
        &self.fields
    }
    pub fn pin_names_visible(&self) -> bool {
        self.show_names
    }
    pub fn pin_numbers_visible(&self) -> bool {
        self.show_numbers
    }
    /// Ensures some basic parameters (requested font sizes, etc.)
    /// are sane
    ///
    /// Returns false if they are all OK.
    pub fn has_dubious_display_params(&self) -> bool {
        for pin in &self.drawing {
            if let DrawingElement::Pin {numsize, ..} = pin {
                if *numsize < 0.1 || *numsize > 200.0 {
                    return true
                }
            }
        }
        false
    }
    /// Ensures all fields, names, etc., etc., do not contain
    /// control characters or other unwanted characters
    ///
    /// If such a field, etc., is found, it is returned. Otherwise,
    /// if all is well, None is returned.
    pub fn validate_strings(&self) -> Option<String> {
       if let Some(x) = validate_string(&self.name) {
           return Some(x);
       }
       if let Some(prefix) = &self.prefix {
           if let Some(x) = validate_string(prefix) {
               return Some(x);
           }
       }
       for alias in self.aliases.iter().chain(self.footprints.iter()) {
           if let Some(x) = validate_string(alias) {
               return Some(x);
           }
       }
       for field in &self.fields {
           if let Some(x) = validate_string(&field.name) {
               return Some(x);
           }
           if let Some(x) = validate_string(&field.contents) {
               return Some(x);
           }
       }
       for pin in &self.drawing {
            if let DrawingElement::Pin {name, number, ..} = pin {
                if let Some(x) = validate_string(name) {
                    return Some(x);
                }
                if let Some(s) = number {
                    if let Some(x) = validate_string(s) {
                        return Some(x);
                    }
                }
            }
       }
       None
    }
}

#[derive(Clone, Debug)]
pub struct LibraryFile {
    pub symbols: Vec<Symbol>,
    version_major: u32,
    version_minor: u32,
    timestamp: Option<String>,
}

// #######  PARSER  #######

pub fn parse_library(input: &str) -> Result<LibraryFile, String> {
    let input = Span::new(input);
    match parse_library_file(input).finish() {
        Ok((rem, out)) => {
            if rem.len() > 0 {
                return Err(format!(
                    "Failed to completely parse library. Bad portion:\n{}",
                    rem
                ));
            }
            Ok(out)
        }
        Err(e) => Err(e.to_string()),
    }
}

fn parse_library_file<'a>(input: Span<'a>) -> IResult<Span<'a>, LibraryFile> {
    let (input, parts) = tuple((
        tag("EESchema-LIBRARY Version"), //0
        space1,                          //1
        text_u32,                        //2
        tag("."),                        //3
        text_u32,                        //4
        opt(tuple((
            //5
            space1,                                            //0
            tag("Date:"),                                      //1
            space1,                                            // 2
            take_till1(|c| c == '#' || c == ' ' || c == '\n'), //3
        ))),
        line_end_with_comments, //6
        many0(parse_symbol),    //7
    ))(input)?;
    Ok((
        input,
        LibraryFile {
            symbols: parts.7,
            version_major: parts.2,
            version_minor: parts.4,
            timestamp: parts.5.map(|s| s.3.to_string()),
        },
    ))
}

fn line_end_with_comments<'a>(input: Span<'a>) -> IResult<Span<'a>, Vec<Span<'a>>> {
    let (input, parts) = many1(tuple((
        opt(tuple((tag("#"), take_till(|c| c == '\n' || c == '\r')))), //comment
        line_ending,
    )))(input)?;
    let mut out = vec![];
    for part in parts {
        if let Some(comment) = part.0 {
            out.push(comment.1)
        }
    }
    Ok((input, out))
}

fn parse_symbol<'a>(input: Span<'a>) -> IResult<Span<'a>, Symbol> {
    let (input, parts) = tuple((
        tag("DEF"),               //0
        space1,                   //1
        take_till1(|c| c == ' '), //2
        space1,                   //3
        take_till1(|c| c == ' '), //4
        space1,                   //5
        tuple((
            //6
            tag("0"),     //6.0
            space1,       // 6.1
            parse_thou,   //6.2
            space1,       //6.3
            one_of("YN"), //6.4
            space1,       //6.5
            one_of("YN"), //6.6
            space1,       //6.7
            text_u32,     //6.8
        )),
        opt(tuple((
            //7
            space1,
            one_of("LF"),
        ))),
        opt(tuple((
            //8
            space1,
            one_of("PN"),
        ))),
        line_end_with_comments, //9
        many1(parse_field),     //10
        opt(tuple((
            //11
            tag("ALIAS"),
            space1,
            many0(tuple((take_till1(|c| c == ' ' || c == '\n'), space0))),
            line_end_with_comments,
        ))),
        opt(tuple((
            //12
            tag("$FPLIST"),
            line_end_with_comments,
            many0(tuple((
                space0,
                take_till1(|c| c == '\n' || c == '$'),
                line_end_with_comments,
            ))),
            tag("$ENDFPLIST"),
            line_end_with_comments,
        ))),
        parse_drawing,          //13
        tag("ENDDEF"),          //14
        line_end_with_comments, //15
    ))(input)?;
    let prefix = if parts.4.starts_with('~') {
        None
    } else {
        Some(parts.4.to_string())
    };
    let aliases = parts.11.map_or(vec![], |alias_tups| {
        let mut aliases = vec![];
        for alias in alias_tups.2 {
            aliases.push(alias.0.to_string());
        }
        aliases
    });
    let fplist = parts.12.map_or(vec![], |fp_tups| {
        let mut fps = vec![];
        for fp in fp_tups.2 {
            fps.push(fp.1.to_string());
        }
        fps
    });
    Ok((
        input,
        Symbol::new(
            parts.2.to_string(), aliases, parts.10, prefix, fplist, parts.6 .2,
            yn_to_bool(parts.6 .4), yn_to_bool(parts.6 .6), parts.6 .8,
            parts.7.map_or(false, |t| to_bool('L', 'F', t.1)),
            parts.8.map_or(false, |t| to_bool('P', 'N', t.1)),
            parts.13,
        ),
    ))
}

fn yn_to_bool(inp: char) -> bool {
    to_bool('Y', 'N', inp)
}
fn to_bool(a: char, b: char, inp: char) -> bool {
    if a == inp {
        true
    } else if b == inp {
        false
    } else {
        unreachable!("Mis-matched to_bool and one_of calls!");
    }
}

fn parse_field<'a>(input: Span<'a>) -> IResult<Span<'a>, Field> {
    let (input, parts) = tuple((
        tag("F"),     //0
        text_u32,     //1
        space1,       //2
        parse_quoted, //3
        space1,       //4
        parse_thou,   //5
        space1,       //6
        parse_thou,   //7
        space1,       //8
        float,        //9
        space1,       //10
        tuple((
            //11
            one_of("HV"), //11.0
            space1,
            one_of("IV"),  //11.2
            space1,        //14
            one_of("CLR"), //11.4
            space1,        //16
            one_of("CTB"), //11.6
            space0,        //11.7
            one_of("IN"),  //11.8
            space0,        //11.9
            one_of("BN"),  //11.10
        )),
        opt(tuple((
            //12
            space1,       //0
            parse_quoted, //1
        ))),
        line_end_with_comments, //13
    ))(input)?;
    let name = match (parts.1, parts.12) {
        (_, Some(tup)) => tup.1,
        (0, None) => "Ref".to_string(),
        (1, None) => "Name".to_string(),
        (2, None) => "Footprint".to_string(),
        (3, None) => "Datasheet".to_string(),
        (_, None) => "Undefined".to_string(),
    };
    Ok((
        input,
        Field {
            name: name,
            contents: parts.3,
            x: parts.5,
            y: -parts.7,
            size: parts.9,
            horizontal: to_bool('H', 'V', parts.11 .0),
            visible: to_bool('V', 'I', parts.11 .2),
            italic: to_bool('I', 'N', parts.11 .8),
            bold: to_bool('B', 'N', parts.11 .10),
            alignment: to_alignment(parts.11 .4, parts.11 .6),
        },
    ))
}

fn parse_quoted<'a>(input: Span<'a>) -> IResult<Span<'a>, String> {
    let (input, parts) = delimited(
        tag("\""),
        alt((
            escaped_transform(
                is_not("\"\\\n"),
                '\\',
                alt((value("\\", tag("\\")), value("\"", tag("\"")))),
            ),
            success("".to_string()),
        )),
        tag("\""),
    )(input)?;
    Ok((input, parts.to_string()))
}

fn to_alignment(h: char, v: char) -> (HorizontalAlignment, VerticalAlignment) {
    use HorizontalAlignment as ha;
    use VerticalAlignment as va;
    let horiz = if h == 'C' {
        ha::Center
    } else if h == 'R' {
        ha::Right
    } else if h == 'L' {
        ha::Left
    } else {
        unreachable!("Mismatched one_of and to_alignment");
    };
    let vert = if v == 'C' {
        va::Center
    } else if v == 'T' {
        va::Top
    } else if v == 'B' {
        va::Bottom
    } else {
        unreachable!("Mismatched one_of and to_alignment: wanted CTB, got {}", v);
    };
    (horiz, vert)
}

fn parse_drawing<'a>(input: Span<'a>) -> IResult<Span<'a>, Vec<DrawingElement>> {
    let (input, parts) = tuple((
        tag("DRAW"),
        line_end_with_comments,
        many0(alt((
            parse_geom_circle,
            parse_geom_arc,
            parse_geom_rect,
            parse_geom_poly,
            parse_pin,
            parse_unimplemented,
        ))),
        tag("ENDDRAW"),
        line_end_with_comments,
    ))(input)?;
    let mut elts = vec![];
    for el in parts.2 {
        elts.push(el);
    }
    Ok((input, elts))
}

fn parse_geom_circle<'a>(input: Span<'a>) -> IResult<Span<'a>, DrawingElement> {
    let (input, parts) = tuple((
        tag("C"),               //0
        space1,                 //1
        parse_thou,             //x,2
        space1,                 //3
        parse_thou,             //y,4
        space1,                 //5
        parse_thou,             //r,6
        space1,                 //7
        text_u32,               //part,8
        space1,                 //7
        text_u32,               //dmg,8
        space1,                 //9
        parse_thou,             //pen,10
        space1,                 //11
        one_of("fFN"),          //fill,12
        line_end_with_comments, //13
    ))(input)?;
    Ok((
        input,
        DrawingElement::Geom {
            geom: GeometryElement::Circle(parts.2, -parts.4, parts.6),
            thickness: parts.12,
            shape_index: if parts.8 == 0 { None } else { Some(parts.8) },
            alt_index: if parts.10 == 0 { None } else { Some(parts.10) },
            fill: parts.14.into(),
        },
    ))
}

fn parse_geom_arc<'a>(input: Span<'a>) -> IResult<Span<'a>, DrawingElement> {
    let (input, parts) = tuple((
        tag("A"),   //0
        space1,     //1
        parse_thou, //x,2
        space1,     //3
        parse_thou, //y,4
        space1,     //5
        parse_thou, //r,6
        space1,
        float, //start,8
        space1,
        float, //end,10
        space1,
        text_u32, //part,12
        space1,
        text_u32, //dmg,14
        space1,
        parse_thou, //pen,16
        space1,
        one_of("fFN"), //fill,18
        many0(
            //ignored starts + ends, 19
            tuple((space1, parse_thou)),
        ),
        line_end_with_comments, //20
    ))(input)?;
    use std::f32::consts::PI;
    fn deciangle_to_rad(x: f32) -> f32 {
        x / 10.0 / 360.0 * 2.0 * PI
    }
    let (mut end, mut start) = // the format is CCW, but this program is CW.
        (deciangle_to_rad(parts.8), deciangle_to_rad(parts.10));
    if start - end < 0.0 {
        let t = end;
        end = start;
        start = t;
    }
    start %= 2.0 * PI;
    end %= 2.0 * PI;
    Ok((
        input,
        DrawingElement::Geom {
            geom: GeometryElement::Arc(parts.2, -parts.4, parts.6, start, end),
            thickness: parts.16,
            shape_index: if parts.12 == 0 { None } else { Some(parts.12) },
            alt_index: if parts.14 == 0 { None } else { Some(parts.14) },
            fill: parts.18.into(),
        },
    ))
}

impl From<char> for FillType {
    fn from(c: char) -> Self {
        use FillType::*;
        match c {
            'f' => Background,
            'F' => Pen,
            'N' => Unfilled,
            _ => unimplemented!("Mismatched one_of for FillType"),
        }
    }
}

fn parse_geom_rect<'a>(input: Span<'a>) -> IResult<Span<'a>, DrawingElement> {
    let (input, parts) = tuple((
        tag("S"),               //0
        space1,                 //1
        parse_thou,             //x,2
        space1,                 //3
        parse_thou,             //y,4
        space1,                 //5
        parse_thou,             //x2,6
        space1,                 //7
        parse_thou,             //y2,8
        space1,                 //9
        text_u32,               //part,10
        space1,                 //11
        text_u32,               //dmg,12
        space1,                 //13
        parse_thou,             //pen,14
        space1,                 //15
        one_of("fFN"),          //fill,16
        line_end_with_comments, //17
    ))(input)?;
    Ok((
        input,
        DrawingElement::Geom {
            geom: GeometryElement::Rect(parts.2, -parts.4, parts.6, -parts.8),
            thickness: parts.14,
            shape_index: if parts.10 == 0 { None } else { Some(parts.10) },
            alt_index: if parts.12 == 0 { None } else { Some(parts.12) },
            fill: parts.16.into(),
        },
    ))
}

fn parse_geom_poly<'a>(input_o: Span<'a>) -> IResult<Span<'a>, DrawingElement> {
    let (input, parts) = tuple((
        tag("P"), //0
        space1,   //1
        text_u32, //count,2
        space1,   //3
        text_u32, //part,4
        space1,   //5
        text_u32, //dmg,6
        space1,
        parse_thou,                                             //pen,8
        many0(tuple((space1, parse_thou, space1, parse_thou))), //9
        space1,
        one_of("fFN"),          //fill,11
        line_end_with_comments, //12
    ))(input_o)?;
    // TODO better errors
    let closing: u32 = parts.9.len().try_into().unwrap();
    if parts.2 != closing {
        return Err(nom::Err::Failure(nom::error::Error::new(
            input_o,
            nom::error::ErrorKind::Count,
        )));
    }
    let mut points = vec![];
    for tup in parts.9 {
        points.push((tup.1, -tup.3));
    }
    Ok((
        input,
        DrawingElement::Geom {
            geom: GeometryElement::Polygon(points),
            thickness: parts.8,
            shape_index: if parts.4 == 0 { None } else { Some(parts.4) },
            alt_index: if parts.6 == 0 { None } else { Some(parts.6) },
            fill: parts.11.into(),
        },
    ))
}

fn parse_pin<'a>(input: Span<'a>) -> IResult<Span<'a>, DrawingElement> {
    let (input, parts) = tuple((
        tag("X"), //0
        space1,   //1
        tuple((
            //2
            opt(tag("~")),   //overbar,2.0
            take_until(" "), //name,2.1
        )),
        space1,           //3
        take_until1(" "), //number,4
        space1,           //5
        parse_thou,       //x,6
        space1,           //7
        parse_thou,       //y,8
        space1,
        parse_thou, //length,10
        space1,
        one_of("UDLR"), //orientation,12
        space1,
        float, //numsize,14
        space1,
        float, //namesize,16
        space1,
        tuple((
            //18
            text_u32, //part,18.0
            space1,
            text_u32, //dmg,18.2
            space1,
            one_of("IOBTPCENUWw"), //type,18.4
        )),
        tuple((
            //19
            space0,
            opt(one_of("N")),
            opt(none_of("\n #")),
        )),
        line_end_with_comments, //20
    ))(input)?;
    Ok((
        input,
        DrawingElement::Pin {
            name: parts.2 .1.to_string(),
            overbar: parts.2 .0.is_some(),
            number: if *parts.4.fragment() == "~" {
                None
            } else {
                Some(parts.4.to_string())
            },
            x: parts.6,
            y: -parts.8,
            length: parts.10,
            orientation: parts.12.into(),
            numsize: parts.14,
            namesize: parts.16,
            shape: (parts.19 .1, parts.19 .2).into(),
            typ: parts.18 .4.into(),
            shape_index: if parts.18 .0 == 0 {
                None
            } else {
                Some(parts.18 .0)
            },
            alt_index: if parts.18 .2 == 0 {
                None
            } else {
                Some(parts.18 .2)
            },
        },
    ))
}

impl From<char> for PinOrientation {
    fn from(c: char) -> Self {
        use PinOrientation::*;
        match c {
            'U' => Up,
            'D' => Down,
            'L' => Left,
            'R' => Right,
            _ => unimplemented!("Mismatched one_of for PinOrientation"),
        }
    }
}

impl From<char> for PinType {
    fn from(c: char) -> Self {
        use PinType::*;
        match c {
            'I' => Input,
            'O' => Output,
            'B' => Bidirectional,
            'T' => Tristate,
            'P' => Passive,
            'C' => OpenCollector,
            'E' => OpenEmitter,
            'N' => NonConnected,
            'W' => PowerInput,
            'w' => PowerOutput,
            'U' => Unspecified,
            _ => unimplemented!("Mismatched one_of for PinType"),
        }
    }
}
impl From<(Option<char>, Option<char>)> for PinGeom {
    fn from(c: (Option<char>, Option<char>)) -> Self {
        use PinShape::*;
        let s = if let Some(c_i) = c.1 {
            let shape = match c_i {
                'I' => Inverted,
                'C' => Clock,
                'L' => LowActive,
                'V' => OutputLow,
                _ => Other(c_i),
            };
            let invisible = if let Some(flag) = c.0 {
                flag == 'N'
            } else {
                false
            };
            PinGeom { invisible, shape }
        } else if let Some(c_o) = c.0 {
            PinGeom {
                shape: Line,
                invisible: c_o == 'N',
            }
        } else {
            PinGeom {
                shape: Line,
                invisible: false,
            }
        };
        s
    }
}

fn parse_unimplemented<'a>(input: Span<'a>) -> IResult<Span<'a>, DrawingElement> {
    let (input, parts) = tuple((take_until1("\n"), line_end_with_comments))(input)?;
    if *parts.0.fragment() == "ENDDRAW" {
        Err(nom::Err::Error(nom::error::Error::new(
            parts.0,
            nom::error::ErrorKind::IsNot,
        )))
    } else {
        Ok((input, DrawingElement::Unimplemented(parts.0.to_string())))
    }
}

/// Parses a string-formatted integer number of thousandths of an inch
/// into decoded nanometers
fn parse_thou<'a>(input: Span<'a>) -> IResult<Span<'a>, i64> {
    let (input, thou) = text_i32(input)?;
    Ok((input, thou_to_nm(thou)))
}

impl Symbol {
    /// Converts this legacy EESchema symbol into multyschema format symbols.
    ///
    /// Not guaranteed to be a perfect conversion; results may be cosmetically
    /// different. For legacy symbols with multiple electrically incompatiable
    /// variants, this produces multiple symbols.
    pub fn into_future(&self) -> Vec<crate::symbol::Symbol> {
        // Scan over drawing to gather electrically useful parts
        // and convert drawing elements
        //
        // EEschema symbols are more like a bunch of symbols hiding in a
        // trenchcoat. We'll treat them all as different, then unifying them
        // (if we can) later.
        //
        // We use four parallel vectors to track per-variant quantities.
        let mut terminals = vec![]; // vec-of-vec of each variant's terminals
        let mut next_ids = vec![]; // counter to assign terminal indicies
        let mut variant_codes = vec![]; // unique codes we generate to encode
        // both shape index and alt index
        let mut variants = vec![]; // vec-of-vec of each variant's path elts
        for elt in &self.drawing {
            match elt {
                DrawingElement::Pin {
                    name, x, y, overbar, length, typ, 
                    shape_index, alt_index, orientation: orient, ..
                } => {
                    let var = to_var_code(shape_index, alt_index);
                    let var_i = match variant_codes.iter()
                        .position(|x| *x==var){
                            Some(i) => i,
                            None => {
                                let i = variant_codes.len();
                                variant_codes.push(var);
                                next_ids.push(0);
                                variants.push(vec![]);
                                terminals.push(vec![]);
                                i
                            }
                    };
                    // Electrical information
                    let offset = na::Vector2::new(*x, *y);
                    let id = next_ids[var_i];
                    next_ids[var_i] += 1;
                    terminals[var_i].push(Terminal::new(
                        name.clone(),
                        *typ,
                        offset,
                        id,
                    ));
                    // Graphical information
                    // TODO: use the shape to call a shape macro here
                    let (lx, ly) = match orient {
                        PinOrientation::Up => (0, -length),
                        PinOrientation::Down => (0, *length),
                        PinOrientation::Right => (*length, 0),
                        PinOrientation::Left => (-length, 0),
                    };
                    let l = na::Vector2::new(lx, ly);
                    variants[var_i]
                        .push(future::Graphic::Path(
                                vec![
                                    future::PathElement::Move(offset),
                                    future::PathElement::Line(offset+l),
                                ],
                                future::PathStyle::primary_default(),
                                )
                            );
                    // add appropriate text graphic element
                    variants[var_i]
                        .push(future::Graphic::Text {
                            loc: offset,
                            content: future::TextEntry::Terminal(id),
                            style: future::TextStyle::terminal_default(
                                orient, *overbar),
                        });
                },
                DrawingElement::Geom {
                    geom, thickness, shape_index, alt_index, fill
                } => {
                    let var = to_var_code(shape_index, alt_index);
                    let var_i = match variant_codes.iter().position(|x| *x==var){
                        Some(i) => i,
                        None => {
                            let i = variant_codes.len();
                            variant_codes.push(var);
                            next_ids.push(0);
                            variants.push(vec![]);
                            terminals.push(vec![]);
                            i
                        }
                    };
                    let weight = (thickness/50000).abs().clamp(1, 255) as u8;
                    let fill = match fill {
                        FillType::Background => Some(future::Color::Tertiary),
                        FillType::Pen => Some(future::Color::Primary),
                        FillType::Unfilled => None,
                    };
                    use GeometryElement::*;
                    let closed_style = future::PathStyle::new(
                         Some(future::Color::Primary),
                         fill, future::Join::Miter, weight, true,
                    );
                    let (path, style) = match geom {
                        Rect(x1, y1, x2, y2) => {
                            let (a, b, c, d) = (
                                na::Vector2::new(*x1, *y1),
                                na::Vector2::new(*x1, *y2),
                                na::Vector2::new(*x2, *y2),
                                na::Vector2::new(*x2, *y1),
                                );
                            (vec![
                                future::PathElement::Move(a),
                                future::PathElement::Line(b),
                                future::PathElement::Line(c),
                                future::PathElement::Line(d),
                            ],
                            closed_style )
                        },
                        Circle(x, y, radius) => {
                            let center = na::Vector2::new(*x, *y);
                            let radius = radius.abs() as u64;
                            (vec![
                             future::PathElement::Arc{
                                 center,
                                 radius,
                                 start: 0,
                                 end: 1_000_000,
                             },
                            ],
                            closed_style
                            )
                        },
                        Arc(x, y, radius, s, e) => {
                            let center = na::Vector2::new(*x, *y);
                            let radius = radius.abs() as u64;
                            let tau = 2.0*std::f32::consts::PI;
                            let start = (s*1.0e6/tau).round() as u32; 
                            let end = (e*1.0e6/tau).round() as u32; 
                            (vec![
                             future::PathElement::Arc{
                                 center,
                                 radius,
                                 start,
                                 end,
                             }
                             ],
                             future::PathStyle::new(
                                 Some(future::Color::Primary),
                                 fill, future::Join::Miter, weight, true, 
                            ))
                        },
                        Polygon(points) => {
                            let mut first = None;
                            let mut closed = true;
                            let mut out = vec![];
                            for point in points {
                                let (x, y) = point;
                                if let Some((fx, fy)) = first {
                                    closed = fx == x && fy == y;
                                    out.push(future::PathElement::Line(
                                            na::Vector2::new(*x, *y)
                                            ));
                                } else {
                                    first = Some((x, y));
                                    out.push(future::PathElement::Move(
                                            na::Vector2::new(*x, *y)
                                            ));
                                }
                            }
                            (out, future::PathStyle::new(
                                Some(future::Color::Primary),
                                fill,
                                future::Join::Miter,
                                weight,
                                closed,
                            ))
                        },
                    };
                    variants[var_i].push(future::Graphic::Path(path, style));
                },
                DrawingElement::Unimplemented(s) => {
                    eprintln!(
                        "While converting, skipped unparseable shape:\n\t {}",
                        s);
                    post_to_user(UserMessage::BadConversion(s.clone()));
                },
            }
        }
        // Time to unify!
        // Two alternate IDs are electrically unifiable if they have the
        // same terminals. Two alternate IDs are fully unifiable if they
        // have the same graphics, too.
        for x in terminals.iter_mut() {
            x.sort();
        }
        let unique_terminals: HashSet<_> = terminals
            .iter().collect();
        // Ok, and now, we can use our handy deduplicated lists to recover
        // which electrically distinct ones actually need to be produced
        let mut out = vec![];
        for t in unique_terminals {
            let e_i = terminals.iter().position(|x| x==t).unwrap();
            // collect the graphic variants up into a list
            // TODO
            if terminals[e_i].len() == 0 {
                continue;
            }
            let graphics = if terminals[0].len() == 0 {
                let mut g = variants[e_i].clone();
                g.extend(variants[0].iter().map(|x| x.clone()));
                g
            } else {
                variants[e_i].clone()
            };
            out.push(future::Symbol::new(
                terminals[e_i].clone(),
                graphics,
                self.name.clone(),
            ));
        }
        out
    }
}

fn to_var_code(alt: &Option<u32>, var: &Option<u32>) -> (u8, u64) {
    let (a, b) = match alt {
        Some(x) => (1, *x as u64),
        None => (0, 0),
    };
    let (c, d) = match var {
        Some(x) => (2, *x as u64),
        None => (0, 0),
    };
    (a+c, b + (d>>32))
}
