#[cfg(feature = "gui")]
mod gui;
mod model;
mod legacy_symbol;
mod symbol;
mod default_library;
#[cfg(feature = "rendering")]
mod view;
mod storage;
#[cfg(feature = "cli")]
mod cli;

#[cfg(feature = "gui")]
use crate::gui::init;

fn main() {
    init_cli();
    init();
}



#[cfg(feature = "cli")]
fn init_cli() {
    cli::run();
}

#[cfg(not(feature = "cli"))]
fn init_cli() {}

#[cfg(not(feature = "gui"))]
fn init() {}

#[cfg(not(feature = "gui"))]
fn post_to_user(message: UserMessage) {
    // TODO CLI
    eprintln!("{:?}", message);
}

#[cfg(feature = "gui")]
/// Shows an error or status message to the user, at a global
/// level.
fn post_to_user(message: UserMessage) {
    crate::gui::show(message);
}

#[derive(Debug, Clone)]
/// Messages to show through a top-level non-diagnoistic communication channel
pub enum UserMessage {
    UiReady,
    ConnectLoop,
    ZeroLength,
    BadLibrary(String),
    BadFile(String),
    BadConversion(String),
    // Uses string to avoid needing simulation error type
    SimFailure(String),
}

// Please consider these strings placeholders! Eventually these
// will be sent through glib internationalization.
impl UserMessage {
    pub fn to_string(&self) -> String {
        match self {
            Self::UiReady => {
                "Multyschema ready!".to_string()
            },
            Self::ConnectLoop => {
                "You cannot connect a loop of wires, because it leads to messy
                diagrams and also it breaks the datastructure this program uses
                for storing them. Nice try!".to_string()
            },
            Self::ZeroLength => {
                "You cannot make a zero-length wire!".to_string()
            },
            Self::BadLibrary(s) => {
                format!("Library was malformed and did not load: {}", s)
            },
            Self::BadFile(s) => {
                format!("Stored schematic file was malformed and \
                        failed to load: {}", s)
            },
            Self::BadConversion(s) => {
                format!("While converting, skipped unparseable shape:\n\t {}",
                        s)
            },
            Self::SimFailure(s) => {
                format!("Simulation failed: {}", s)
            }
        }
    }
}
