/// subsystem for GTK+3 graphical interface
///
/// NOT threadsafe, contains global variables
use gtk::prelude::*;
use gtk::*;
use glib::source;
use lazy_static::lazy_static;
use std::boxed::Box;
use std::collections::{BTreeMap, VecDeque};
use std::env::args;
use std::fs;
use std::path::PathBuf;
use std::sync::Mutex;

use crate::{post_to_user, UserMessage};
use crate::model::*;
use crate::symbol::*;
use crate::view::Viewport;
use crate::view::*;
use crate::storage::{load_json, save_json, load_binary_file, save_binary_file};
use crate::legacy_symbol::parse_library;
use crate::default_library;
use nalgebra::{Vector2, Point2};

#[cfg(feature = "sim")]
mod sim;
#[cfg(feature = "sim")]
use sim::*;
#[cfg(feature = "sim")]
use plotters_cairo::CairoBackend;

lazy_static! {
    static ref SYMBOL_TABLE: Mutex<BTreeMap<String, Symbol>> = Mutex::new(BTreeMap::new());
    static ref TOPSHEET: Mutex<Sheet> = Mutex::new(Sheet::empty_default());
    static ref STATE: Mutex<State> = Mutex::new(State::Waiting);
    static ref VIEWPORT: Mutex<Viewport> = Mutex::new(Viewport::new());
    static ref MESSAGEBOX: Mutex<MessageBox> = Mutex::new(MessageBox::new());
}
#[cfg(feature = "sim")]
thread_local! {
    static SIMTHREAD: Mutex<SimulationThread> = Mutex::new(SimulationThread::Uninitialized);
    static SIMPLOT: Mutex<Option<DrawingArea>> = Mutex::new(None);
}

fn decode_draw_params(param: &[glib::Value]) -> (DrawingArea, cairo::Context) {
    if param.len() < 2 {
        panic!("Too few arguments to redraw handler");
    }
    let wi: DrawingArea = param[0].get().unwrap();
    let c: cairo::Context = param[1].get().unwrap();

    (wi, c)
}

fn draw_schematic(param: &[glib::Value]) -> Option<glib::Value> {
    let (widget, ctx) = decode_draw_params(param);
    let state = STATE.lock().unwrap();
    let mut v = VIEWPORT.lock().unwrap();
    v.update_from(&widget);
    match &*state {
        State::Waiting | State::Panning(_,_) | State::WireInit => {
            let sheet = TOPSHEET.lock().unwrap();
            redraw_schematic(&v, &ctx, &*sheet)
        }
        State::Hovering(mname, x, y) => {
            let table = SYMBOL_TABLE.lock().unwrap();
            let s = table
                .get(mname)
                .expect("Failed to find specified symbol in global table?");
            let sheet = TOPSHEET.lock().unwrap();
            redraw_schematic(&v, &ctx, &*sheet);
            draw_held_symbol(&v, &ctx, s, *x, *y)
        }
        State::WireStarting(from) => {
            let sheet = TOPSHEET.lock().unwrap();
            redraw_schematic(&v, &ctx, &*sheet);
            draw_wire_cursor(&v, &ctx, *from)
        }
        State::WireConnecting(from, lastto, flop) => {
            let sheet = TOPSHEET.lock().unwrap();
            redraw_schematic(&v, &ctx, &*sheet);
            draw_held_wire(&v, &ctx, *from, *lastto, *flop)
        }
    }
}

fn loadlibrary(path: PathBuf, select_table: TreeStore) {
    // Go look for a library
    // TODO make actually pick a file
    //let path = "/usr/share/kicad/library/74xGxx.lib";
    //let path = "truncated_lib.kilib";
    let filename = path.file_stem().unwrap().to_str().unwrap();
    // Make symbols exist
    let file = fs::read_to_string(path.clone()).unwrap();
    let lib = match parse_library(&file) {
        Ok(x) => x,
        Err(e) => {
            let mut e = e.to_string();
            e.truncate(50);
            post_to_user(UserMessage::BadLibrary(e));
            return;
        }
    };
    // Iterate over the returned symbols
    let mut table = SYMBOL_TABLE.lock().unwrap();
    for symbol in lib.symbols {
        // TODO: actually make this mangle gud
        let mut symbols = symbol.into_future();
        for (i, symbol) in symbols.drain(..).enumerate() {
            let mangled_name = format!("{}_{}_{}", 
                                       filename,
                                       symbol.name(),
                                       i);
            let row = select_table.append(None);
            select_table.set_value(&row, 0, &symbol.name().into());
            select_table.set_value(&row, 1, &filename.into());
            select_table.set_value(&row, 2, &(&mangled_name).into());
            table.insert(mangled_name, symbol);
        }
    }
}

fn get_symbol_mname(view: &TreeView, store: &TreeStore) -> Option<String> {
    // Get selected element
    let (path, _) = view.cursor();
    let path = match path {
        Some(p) => p,
        None => return None,
    };
    let row = match store.iter(&path) {
        Some(l) => l,
        None => {
            eprintln!("Failed to find node");
            return None;
        }
    };
    let mangled_name: String = store
        .value(&row, 2)
        .get()
        .expect("Bad datatype in tree model");
    Some(mangled_name)
}

pub fn init() -> i32 {
    let application = gtk::Application::new(Some("com.groomble.multyschema"), Default::default());

    application.connect_activate(|app| {
        // Load the compiled resource bundle
        let resources_bytes = include_bytes!("../resources/resources.gresource");
        let resource_data = glib::Bytes::from(&resources_bytes[..]);
        let res = gio::Resource::from_data(&resource_data).unwrap();
        gio::resources_register(&res);

        // Load the window UI
        let builder = Builder::from_resource("/com/groomble/multyschema/main.glade");

        // Get a reference to the window
        let window: ApplicationWindow = builder.object("main").expect("Couldn't get window");
        window.set_application(Some(app));

        // part picker dialog
        let part_select_dialog: Dialog = builder
            .object("part_select_dialog")
            .expect("Couldn't get part dialog");
        // symbol backing store for picker dialog
        let part_select_symbols: TreeStore = builder
            .object("symbols")
            .expect("Couldn't get part picker store");
        // symbol view for picker
        let part_select_view: TreeView = builder
            .object("symbol_select_tree")
            .expect("Couldn't get part picker view");
        let part_select_preview_area: DrawingArea = builder
            .object("preview_area")
            .expect("Couldn't get part picker preview drawable");
        let library_filechooser: FileChooserDialog = builder
            .object("library_filechooser")
            .expect("Couldn't get 'add library' file picker");
        // Add buttons to work around Glade strangeness
        library_filechooser.add_buttons(&[
                                        ("Add", ResponseType::Ok),
                                        ("Cancel", ResponseType::Cancel),
        ]);
        // Add default KiCAD library location, for convenience (ok if fails)
        let _ = library_filechooser
            .add_shortcut_folder("/usr/share/kicad/library");
        let file_open_dialog: FileChooserDialog = builder
            .object("file_open_dialog")
            .expect("Couldn't get file open dialog");
        // Add buttons to work around Glade strangeness
        file_open_dialog.add_buttons(&[
                                     ("Open", ResponseType::Ok),
                                     ("Cancel", ResponseType::Cancel),
        ]);
        let file_save_dialog: FileChooserDialog = builder
            .object("file_save_dialog")
            .expect("Couldn't get file save dialog");
        // Add buttons to work around Glade strangeness
        file_save_dialog.add_buttons(&[
                                     ("Save", ResponseType::Ok),
                                     ("Cancel", ResponseType::Cancel),
        ]);
        let binary_format: CheckMenuItem = builder
            .object("binary_format")
            .expect("Couldn't get binary format toggle");

        let mut status_area: Label = builder
            .object("status_area")
            .expect("Couldn't get status reporting label");
        // Set up refreshing for the status area
        source::timeout_add_seconds_local(1, Box::new(move || {
            let mut mbox = MESSAGEBOX.lock().unwrap();
            mbox.update(&mut status_area);
            source::Continue(true)
        }));
        let sim_plot: DrawingArea = builder.object("sim_plot_area")
            .expect("Couldn't get the simulation plotting area");
        SIMPLOT.with(move |w| {
            let mut x = w.lock().unwrap();
            *x = Some(sim_plot)
        });
        // Load default library set for the user to pick from
        default_libraries(part_select_symbols.clone());
        // Get the schematic area
        let schematic: DrawingArea = builder.object("schematic").expect("Couldn't get schematic");
        let schematic_c = schematic.clone();
        builder.connect_signals(move |_builder, handler_name| {
            let schematic = schematic.clone();
            let part_select_dialog_1 = part_select_dialog.clone();
            let part_select_symbols = part_select_symbols.clone();
            let part_select_view = part_select_view.clone();
            let part_select_preview_area = part_select_preview_area.clone();
            let library_filechooser = library_filechooser.clone();
            let file_save_dialog = file_save_dialog.clone();
            let file_open_dialog = file_open_dialog.clone();
            let binary_format = binary_format.clone();
            match handler_name {
                "sim-plot-redraw" => Box::new(move |param| {
                    let (widget, ctx) = decode_draw_params(param);
                    #[cfg(feature = "sim")]
                    {
                        SIMTHREAD.with(|s| {
                            let mut sim_thread = s.lock().unwrap();
                            let wh = (widget.allocated_width() as u32, 
                                      widget.allocated_height() as u32);
                            sim_thread.render_latest(
                                CairoBackend::new(&ctx, wh).unwrap()
                                );
                        });
                    }
                    Some((&false).into())
                }),
                "sim-run" => Box::new(move |_| {
                    #[cfg(feature = "sim")]
                    {
                        SIMTHREAD.with(|s| {
                            let mut sim_thread = s.lock().unwrap();
                            let sheet = TOPSHEET.lock().unwrap();
                            sim_thread.queue_simulation(&sheet);
                        });
                    }
                    None
                }),
                "file_save" => Box::new(move |_| {
                    file_save_dialog.run();
                    None
                }),
                "file_save_response" => Box::new(move |param| {
                    println!("params {:?}", param);
                    let response: i32 = param[1].get().unwrap();
                    let response: ResponseType = response.into();
                    if ResponseType::Ok == response {
                        let file = file_save_dialog.filename()
                            .expect("Sucessful file pickers always have file?");
                        let sheet = TOPSHEET.lock().unwrap();
                        let sheets = vec![&*sheet];
                        if binary_format.is_active() {
                            save_binary_file(file, sheets).unwrap();
                        } else {
                            save_json(file, sheets).unwrap();
                        }
                    } else {
                        eprintln!("Got response {:?}", response);
                    }
                    file_save_dialog.hide();
                    None
                }),
                "file_open" => Box::new(move |_| {
                    file_open_dialog.run();
                    None
                }),
                "file_open_response" => Box::new(move |param| {
                    println!("params {:?}", param);
                    schematic.queue_draw();
                    let response: i32 = param[1].get().unwrap();
                    let response: ResponseType = response.into();
                    if ResponseType::Ok == response {
                        let file = file_open_dialog.filename()
                            .expect("Sucessful file pickers always have file?");
                        let mut sheets = if binary_format.is_active() {
                            match load_binary_file(file) {
                                Ok(f) => f,
                                Err(e) => {
                                    post_to_user(
                                        UserMessage::BadFile(e.to_string()));
                                    return None;
                                }
                            }
                        } else {
                            match load_json(file) {
                                Ok(f) => f,
                                Err(e) => {
                                    post_to_user(
                                        UserMessage::BadFile(e.to_string()));
                                    return None;
                                }
                            }
                        };
                        let mut sheet = TOPSHEET.lock().unwrap();
                        *sheet = sheets.pop().unwrap();
                    } else {
                        eprintln!("Got response {:?}", response);
                    }
                    file_open_dialog.hide();
                    None
                }),
                "loadlibrary" => Box::new(move |_| {
                    // Ask the user to pick the library
                    library_filechooser.run();
                    None
                }),
                "library_filechooser_response" => Box::new(move |param| {
                    println!("params {:?}", param);
                    let response: i32 = param[1].get().unwrap();
                    let response: ResponseType = response.into();
                    if ResponseType::Ok == response {
                        let files = library_filechooser.filenames();
                        schematic.queue_draw();
                        for file in files {
                            loadlibrary(file, part_select_symbols.clone());
                        }
                    } else {
                        eprintln!("Got response {:?}", response);
                    }
                    library_filechooser.hide();
                    None
                }),
                "noconnect" => Box::new(|_| None),
                "connection" => Box::new(|_| {
                    let mut state = STATE.lock().unwrap();
                    // override previous state, no matter what.
                    *state = State::WireInit;
                    None
                }),
                "component" => Box::new(move |_| {
                    part_select_dialog_1.run();
                    None
                }),
                "preview-draw" => Box::new(move |p| {
                    let mangled_name = get_symbol_mname(&part_select_view, &part_select_symbols);
                    let mangled_name = if let Some(l) = mangled_name {
                        l
                    } else {
                        return Some(glib::Value::from(&true));
                    };
                    let table = SYMBOL_TABLE.lock().unwrap();
                    if let Some(symbol) = table.get(&mangled_name) {
                        let (w, ctx) = decode_draw_params(p);
                        let mut v = Viewport::new();
                        v.update_from(&w);
                        redraw_preview(&v, &ctx, &symbol)
                    } else {
                        eprintln!("Skipping drawing preview since couldn't find part!");
                        Some(glib::Value::from(&true))
                    }
                }),
                "schematic-draw" => Box::new(draw_schematic),
                "schematic-mouse-press" => Box::new(|param| {
                    let (widget, e) = unwrap_mouse_params(param);
                    let (x, y) = match e.coords() {
                        Some(c) => c,
                        None => {
                            return Some(glib::Value::from(&false));
                        }
                    };
                    let mut v = VIEWPORT.lock().unwrap();
                    v.update_from(&widget);
                    let coords = v.iform(Point2::new(x, y));
                    let coords = v.snap_to_grid(coords);
                    let mut state = STATE.lock().unwrap();
                    match (e.button(), &mut *state) {
                        (Some(gdk::BUTTON_PRIMARY), State::Hovering(mname, _x, _y)) => {
                            // left click, while holding component -> drop it
                            let mut sheet = TOPSHEET.lock().unwrap();
                            let table = SYMBOL_TABLE.lock().unwrap();
                            let s = table
                                .get(mname)
                                .expect("Failed to find specified symbol in global table?");
                            sheet.insert_symbol(s, mname, coords);
                            *state = State::Waiting;
                            widget.queue_draw();
                            return Some(glib::Value::from(&true));
                        }
                        (Some(gdk::BUTTON_PRIMARY), State::Panning(_, ref mut oldstate)) => {
                            // failsafe: if you somehow click without ending a drag, it ends the
                            // drag when you click.
                            eprintln!("Triggering drag escape failsafe!");
                            // swap out oldstate with dummy value.
                            let oldstate = std::mem::replace(&mut **oldstate, State::Waiting);
                            *state = oldstate;
                        }
                        (Some(gdk::BUTTON_PRIMARY), State::WireStarting(_)|State::WireInit) => {
                            *state = State::WireConnecting(coords, coords, FlopDirection::Undecided);
                        }
                        (Some(gdk::BUTTON_PRIMARY), State::WireConnecting(from, _to, flop)) => {
                            let mut sheet = TOPSHEET.lock().unwrap();
                            let done = sheet.insert_wire(*from, coords, *flop);
                            if done {
                                *state = State::WireInit;
                            } else {
                                *state = State::WireConnecting(
                                    coords, coords, FlopDirection::Undecided);
                            }
                        }
                        (Some(gdk::BUTTON_SECONDARY), State::WireConnecting(..)) => {
                            // cancel out of placing just this wire
                            *state = State::WireStarting(coords);
                        }
                        // in general, right click cancels out of a tool
                        (Some(gdk::BUTTON_SECONDARY), _) => {
                            *state = State::Waiting;
                        }
                        (_, _) => {}
                    }
                    widget.queue_draw();
                    Some(glib::Value::from(&false))
                }),
                "schematic-mouse" => Box::new(|param| {
                    let (widget, e) = unwrap_mouse_params(param);
                    let (x, y) = match e.coords() {
                        Some(c) => c,
                        None => {
                            return Some(glib::Value::from(&false));
                        }
                    };
                    let mut v = VIEWPORT.lock().unwrap();
                    v.update_from(&widget);
                    let loose_coords = v.iform(Point2::new(x, y));
                    let coords = v.snap_to_grid(loose_coords);
                    let nm_coords = v.xform_o(coords);
                    let (x, y) = (nm_coords[0], nm_coords[1]);
                    let mut state = STATE.lock().unwrap();
                    match &*state {
                        State::Waiting => {
                            // empty cursor, not placing a part, do nothing
                        }
                        State::Panning(_,_) => {
                            return Some(glib::Value::from(&false));
                        }
                        State::Hovering(mname, _x, _y) => {
                            // draw held component
                            widget.queue_draw();
                            *state = State::Hovering(mname.to_string(), x, y);
                        }
                        State::WireStarting(_oldcoords) => {
                            // draw wire cursor
                            widget.queue_draw();
                            *state = State::WireStarting(coords);
                        }
                        State::WireConnecting(from, _to, flop) => {
                            // draw in-progress wire
                            widget.queue_draw();
                            let flop = if *flop == FlopDirection::Undecided {
                                if (loose_coords.x - from.x).abs() > (loose_coords.y - from.y).abs() {
                                    FlopDirection::XThenY
                                } else {
                                    FlopDirection::YThenX
                                }
                            } else {
                                *flop
                            };
                            // NOTE: should maybe be or?
                            let flop = if coords.x == from.x && coords.y == from.y {
                                FlopDirection::Undecided
                            } else {
                                flop
                            };
                            *state = State::WireConnecting(*from, coords, flop);
                        },
                        State::WireInit => {
                            *state = State::WireStarting(coords);
                        }
                    }
                    Some(glib::Value::from(&true))
                }),
                "schematic-scroll" => Box::new(|param| {
                    let (widget, e) = unwrap_mouse_params(param);
                    assert_eq!(e.event_type(), gdk::EventType::Scroll);
                    let (x, y) = match e.coords() {
                        Some(c) => c,
                        None => {
                            return Some(glib::Value::from(&false));
                        }
                    };
                    // TODO: smooth scrolling
                    let scale = match e.scroll_direction() {
                        Some(gdk::ScrollDirection::Up) => 1,
                        Some(gdk::ScrollDirection::Down) => -1,
                        Some(_) => 0,
                        None => {
                            eprintln!("This shouldn't be possible?");
                            return Some(glib::Value::from(&false));
                        }
                    };
                    let mut v = VIEWPORT.lock().unwrap();
                    v.scale_about_point(scale as f64, (x, y));
                    widget.queue_draw();
                    Some(glib::Value::from(&true))
                }),
                "part_select_activate" => Box::new(move |p| {
                    assert!(
                        p.len() >= 2,
                        "activate handler must have at least 2 parameters"
                    );
                    let path: TreePath = p[1].get().expect("activate handler must get path");
                    let row = match part_select_symbols.iter(&path) {
                        Some(l) => l,
                        None => {
                            eprintln!("Failed to find node");
                            return None;
                        }
                    };
                    let mangled_name: String = part_select_symbols
                        .value(&row, 2)
                        .get()
                        .expect("Bad datatype in tree model");
                    let mut state = STATE.lock().unwrap();
                    *state = State::Hovering(mangled_name, 0.0, 0.0);
                    part_select_dialog_1.hide();
                    None
                }),
                "part_select_select" => Box::new(move |_| {
                    let mangled_name = get_symbol_mname(&part_select_view, &part_select_symbols);
                    let mangled_name = if let Some(l) = mangled_name {
                        l
                    } else {
                        return None;
                    };
                    let mut state = STATE.lock().unwrap();
                    *state = State::Hovering(mangled_name, 0.0, 0.0);
                    part_select_dialog_1.hide();
                    None
                }),
                "part_select_update" => Box::new(move |_| {
                    part_select_preview_area.queue_draw();
                    None
                }),
                "part_select_cancel" => Box::new(move |_| {
                    part_select_dialog_1.hide();
                    None
                }),
                "scale-format" => Box::new(move |p| {
                    assert!(
                        p.len() >= 2,
                        "format handler must have at least 2 parameters"
                    );
                    let val: f64 = p[1].get().expect("range format callback wrong params?");
                    let ret = format!("{} units", (val-3.0).exp2() * 200.0);
                    Some((&ret).into())
                }),
                "scale-update" => Box::new(move |p| {
                    assert!(
                        p.len() >= 1,
                        "scale update handler must have at least 1 parameters"
                    );
                    let val: Adjustment = p[0].get().expect("scale changed callback wrong params?");
                    let val = val.value();
                    // set grid!
                    let mut v = VIEWPORT.lock().unwrap();
                    let dispgrid = ((val-3.0).exp2() * 200.0).round() as i64;
                    v.set_grid(dispgrid*Viewport::base_grid()/200);
                    None
                }),
                "library_filechooser_destroy" => Box::new(move |_| {
                    let res = library_filechooser.hide_on_delete();
                    Some((&res).into())
                }),

                _ => unimplemented!("Unimplemented handler name: {}", handler_name),
            }
        });

        // Add a off-the-shelf drag controller to the schematic
        let gesture = builders::GestureDragBuilder::new()
            .button(gdk::BUTTON_MIDDLE)
            //.exclusive(false)
            //.touch_only(false)
            //.n_points(1)
            .propagation_phase(PropagationPhase::Bubble)
            .widget(&schematic_c)
            .build();
        // connect signals appropriately
        gesture.connect_drag_begin(Box::new(|_gesture: &GestureDrag, _, _| {
            // store the drag start offset
            let v = VIEWPORT.lock().unwrap();
            // TODO: is a scale-independent offset what we actually want? Seems to 'pop'
            // when zooming now, which is bad.
            let offset = v.offset();
            let mut state = STATE.lock().unwrap();
            let oldstate = std::mem::replace(&mut *state, State::Waiting);
            *state = State::Panning(offset, Box::new(oldstate));
        }));
        let schematic = schematic_c.clone();
        gesture.connect_drag_update(Box::new(move |_gesture: &GestureDrag, x, y| {
            // set the viewport translation
            let mut v = VIEWPORT.lock().unwrap();
            let state = STATE.lock().unwrap();
            match &*state {
                State::Panning(coords, _) => {
                    v.set_offset(coords);
                    v.translate_rel((x, y));
                    schematic.queue_draw();
                },
                _ => {
                    eprintln!("Got into wrong state in middle of drag, somehow.");
                    return; // assume that whatever has happened is fine
                }
            }
        }));
        let schematic = schematic_c.clone();
        gesture.connect_drag_end(Box::new(move |_gesture: &GestureDrag, x, y| {
            // set the viewport translation
            let mut v = VIEWPORT.lock().unwrap();
            let mut state = STATE.lock().unwrap();
            match &mut *state {
                State::Panning(coords, ref mut oldstate) => {
                    v.set_offset(coords);
                    v.translate_rel((x, y));
                    let oldstate = std::mem::replace(&mut **oldstate, State::Waiting);
                    *state = oldstate;
                    schematic.queue_draw();
                },
                _ => {
                    eprintln!("Got into wrong state in middle of drag, somehow.");
                    return; // assume that whatever has happened is fine
                }
            }
        }));
        // make sure the gesture controller isn't destroyed
        unsafe {
            schematic_c.set_data("schematic-drag-controller", gesture);
        };
        // is pretty much the same as
        // std::mem::forget(gesture);
        // don't mind the unsafe -- we never read this back and I'm not sure why _this_
        // is unsafe rather than just the read function.

        // Workaround: force value change signal to ensure no stale un-unitted display
        // of grid factor
        let grid_level_scale: Adjustment = builder
            .object("grid_levels")
            .expect("Couldn't get grid level scale");
        grid_level_scale.set_value(3.0);

        // Show the UI
        window.show_all();
        show(UserMessage::UiReady);
    });

    application.run_with_args(&args().collect::<Vec<_>>())
}

enum State {
    Waiting,
    Hovering(String, f64, f64),
    Panning(Vector2<i64>, Box<State>),
    /// Wire tool selected, but mouse hasn't entered the area yet
    WireInit,
    /// latest value of to 
    WireStarting(Point2<i64>),
    /// from, lastest value of to
    WireConnecting(Point2<i64>, Point2<i64>, FlopDirection),
}

fn unwrap_mouse_params(param: &[glib::Value]) -> (DrawingArea, gdk::Event) {
    if param.len() < 2 {
        panic!("Too few arguments to mouse handler");
    }
    let wi: DrawingArea = param[0].get().unwrap();
    let e: gdk::Event = param[1].get().unwrap();
    (wi, e)
}

pub fn show(message: UserMessage) {
    // put it in the message box
    let mut mbox = MESSAGEBOX.lock().unwrap();
    mbox.push(message);
}

/// Represents the message-box we show to the user
struct MessageBox {
    /// Counter incremented about every second
    counter: u64,
    /// Counter index for when the last notification was shown the the user
    last: u64,
    /// Notification queue
    queue: VecDeque<UserMessage>,
}

impl MessageBox {
    fn new() -> Self {
        MessageBox {
            // Make sure we don't show a stale message
            counter: Self::timeout()+1,
            last: 0,
            queue: VecDeque::new(),
        }
    }
    fn push(&mut self, m: UserMessage) {
        self.queue.push_back(m);
    }
    fn update(&mut self, output: &mut Label) {
        self.counter += 1;
        // updates only needed when we have new elements
        if self.queue.is_empty() {
            return;
        }
        // updates only needed when the user has had a chance to look
        // at the old one
        if self.counter < self.last + Self::timeout() {
            return;
        }
        // Ok, update them
        // TODO: translation code goes here
        let m = self.queue.pop_front().unwrap();
        let remaining = self.queue.len();
        let s = if remaining > 0 {
            format!("({} more) {}", remaining, m.to_string())
        } else {
            format!("{}", m.to_string())
        };
        output.set_text(&s);
    }
    /// Approximate time, in seconds, a user needs to see a notifcation
    fn timeout() -> u64 {
        4
    }
}

fn default_libraries(select_table: TreeStore) {
    let lib = default_library::library();
    let mut table = SYMBOL_TABLE.lock().unwrap();
    for symbol in lib {
        let mangled_name = format!("default_{}", symbol.name());
        let row = select_table.append(None);
        select_table.set_value(&row, 0, &symbol.name().into());
        select_table.set_value(&row, 1, &"default".into());
        select_table.set_value(&row, 2, &(&mangled_name).into());
        table.insert(mangled_name, symbol);
    }
}

#[cfg(feature = "sim")]
fn queue_redraw_sim_plot() {
    SIMPLOT.with(|w| {
        let x = w.lock().unwrap();
        let area = x.as_ref().expect("Redraw queue request before UI init????");
        area.queue_draw();
    });
}
