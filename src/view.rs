//! All rendering code should go here.
//!
//! Right now, this is tightly coupled with Cairo.
//!
#[cfg(feature = "gui")]
use gtk::{
    prelude::WidgetExt,
    DrawingArea,
};

use cairo::{Context, Error, FontSlant, FontWeight};
use glib::Value;

use crate::model::*;

use crate::symbol::*;

use nalgebra as na;
use na::{Point2, Vector2};

use std::ptr;

#[derive(Clone, Copy, Debug)]
pub struct Viewport {
    /// Width of viewport
    width: f64,
    /// Height of viewport
    height: f64,
    /// Transform from sheet-space (nanometer unit) to screen space
    /// (fractional px unit)
    sheet2screen: na::Scale2<f64>,
    /// Transform from absolute sheet-space to viewport sheet-space
    transform: na::Similarity2<f64>,
    /// Grid size
    grid: i64,
}

impl Viewport {
    /// Converts an abstract color into a concrete RGB triple
    pub fn color_lookup(&self, x: &Color) -> (f64, f64, f64) {
        match x {
            Color::Primary => {
                (0.2, 0.5, 0.2)
            },
            Color::Secondary => {
                (0.6, 0.2, 0.6)
            },
            Color::Tertiary => {
                (0.7, 0.7, 0.7)
            },
        }
    }
    /// Takes a sheet location in nanometers to display (application pixel) coordinates
    pub fn xform(&self, nm: &Point2<i64>) -> Point2<f64> {
        let nm = na::Point2::new(nm[0] as f64, nm[1] as f64);
        self.transform * (self.sheet2screen * nm)
    }
    /// Variant for owned values of xform
    pub fn xform_o(&self, nm: Point2<i64>) -> Point2<f64> {
        self.xform(&nm)
    }
    /// Takes a display (application pixel) coordinate to a sheet location in nanometers.
    ///
    /// This is only a right inverse to xform. Honestly, for bigger values, it might not even be that.
    pub fn iform(&self, px: Point2<f64>) -> Point2<i64> {
        let px_abs = self.transform.inverse_transform_point(&px);
        let nm = self.sheet2screen.try_inverse_transform_point(&px_abs)
            .unwrap();
        Point2::new(nm[0].round() as i64, nm[1].round() as i64)

    }
    pub fn dimensions(&self) -> (f64, f64) {
        (self.width, self.height)
    }
    /// Scales the viewport such that the given point does not move
    pub fn scale_about_point(&mut self, clicks: f64, coords: (f64, f64)) {
        let scale_factor = (clicks / 4.0).exp();
        // Scaling about a point is the same as translating so a point is at
        // the origin, scaling, than translating back. That is, t must be stationary,
        // so we do
        // f(x) = (x - t)*a + t
        // so that f(t) = t. This can also be written
        // f(x) = x*a - t*a + t = x*a - t*(a-1)
        // Oh look, a similarity transform!
        let t = self.transform.inverse_transform_point(&na::Point2::new(coords.0, coords.1));
        let mtam1 = -t*(scale_factor - 1.0);
        let offset_over: na::Translation2<f64> = mtam1.into();
        let mut back = na::Similarity2::from_scaling(scale_factor);
        back.append_translation_mut(&offset_over);
        // and then post-multiply onto the current transform and we're done!
        self.transform = self.transform * back;
    }
    pub fn scale(&self, i: &i64) -> f64 {
        // take the average scale of the axis for this
        let pt = Point2::new(*i as f64, *i as f64);
        let screen = self.sheet2screen * pt;
        let avg = (screen.coords[0] + screen.coords[1])/2.0;
        self.transform.scaling()*avg
    }
    /// Vector transform
    pub fn scale_pair(&self, v: &Vector2<i64>) -> Vector2<f64> {
        let v = v.cast();
        self.transform * (&self.sheet2screen * v)
    }
    pub fn set_grid(&mut self, grid: i64) {
        println!("New grid increment: {}", grid);
        self.grid = grid;
    }
    pub fn snap_to_grid(&self, coords: Point2<i64>) -> Point2<i64> {
        Point2::new(
            coords.x-((coords.x+self.grid/2)%self.grid - self.grid/2),
            coords.y-((coords.y+self.grid/2)%self.grid - self.grid/2),
        )
    }
    /// Translates the view by the given amount, in screen coordinates
    pub fn translate_rel(&mut self, by: (f64, f64)) {
        let amt = Vector2::new(by.0, by.1);
        let translation = na::Translation2::from(amt);
        self.transform.append_translation_mut(&translation);
    }
    /// Returns a coordinate pair specifying the viewport offset in
    /// a scale-independent coordinate system.
    pub fn offset(&self) -> Vector2<i64> {
        let translation = self.transform.isometry.translation;
        Vector2::new(
            translation.vector.x.round() as i64,
            translation.vector.y as i64)
    }
    /// Restores an offset coordinate pair made using get_offset
    pub fn set_offset(&mut self, to: &Vector2<i64>) {
        let to = Vector2::new(to.x as f64, to.y as f64);
        self.transform.isometry.translation = to.into();
    }
    #[cfg(feature = "gui")]
    /// Gives a viewport with correct 1-to-1 scaling for the physical dimensions of the monitor
    /// this widget is displayed on. The scale parameter is left at unchanged and can be changed
    /// without losing this physical correspondence.
    pub fn update_from(&mut self, f: &DrawingArea){
        // get toplevel
        let window = f
            .parent_window()
            .expect("Viewports must be made on attached widgets");
        // get monitor
        let display = window.screen().display();
        let monitor = display
            .monitor_at_window(&window)
            .or_else(|| display.primary_monitor())
            .or_else(|| display.monitor(0))
            .expect("Cannot find any attached monitor!");
        // get pixel size
        let geom = monitor.geometry();
        let pix_width = monitor.width_mm() * 1000000/geom.width();
        let pix_height = monitor.height_mm() * 1000000/geom.height();
        let width = f.allocated_width().into();
        let height = f.allocated_height().into();
        // construct defaults
        self.width = width;
        self.height = height;
        // We need a scale factor that will take pixel size (nm) to 1 (pixel)
        // Here's a scale transform that will take pixels -> nm
        let screen2sheet = na::Scale2::new(pix_width as f64, pix_height as f64);
        self.sheet2screen = screen2sheet.try_inverse().unwrap();
    }
    pub fn new() -> Viewport {
        let sheet2screen = na::Scale2::new(1.0/85_000.0, 1.0/85_000.0);
        Viewport {
            width: 1080.0,
            height: 1920.0,
            sheet2screen,
            transform: na::Similarity2::identity(),
            /// Grid size
            grid: Self::base_grid(),
        }
    }
    pub fn base_grid() -> i64 {
        10_000_000/4
    }
}

pub fn redraw_schematic(v: &Viewport, ctx: &Context, sheet: &Sheet) -> Option<glib::Value> {
    // Draw the existing parts of the schematic
    {
        for symbol in sheet.symbols() {
            ctx.save().unwrap();
            let coords = v.xform(&symbol.coords());
            ctx.translate(coords.x, coords.y);
            draw_symbol(v, ctx, symbol.symbol(), Some(symbol)).unwrap();
            ctx.restore().unwrap();
        }
        for node in sheet.nodes() {
            for handle in node.handles() {
                if (!node.is_root(handle) && handle.num_children() > 1) || handle.num_children() > 2 {
                    ctx.set_source_rgb(0.1, 0.5, 0.1);
                    let c = v.xform_o(handle.loc(sheet));
                    ctx.new_path();
                    ctx.arc(c.x, c.y, 5.0, 0.0, 2.0*std::f64::consts::PI);
                    ctx.fill().ok();
                }
                if handle.is_terminal() {
                    ctx.set_source_rgb(0.1, 0.5, 0.1);
                    let c = v.xform_o(handle.loc(sheet));
                    ctx.new_path();
                    ctx.rectangle(c.x-3.0, c.y-3.0, 6.0, 6.0);
                    ctx.fill().ok();
                }
            }
            draw_wire_smart(v, ctx, sheet, node.traverse().map(|x| (x.1, x.2)))
                .unwrap();
        }
    }
    Some(Value::from(&false))
}
pub fn draw_wire_cursor(v: &Viewport, ctx: &Context, from: Point2<i64>)
    -> Option<glib::Value> {
        let from = v.xform_o(from);
        ctx.set_source_rgb(0.05, 0.3, 0.05);
        ctx.new_path();
        ctx.arc(from.x, from.y, 5.0, 0.0, 2.0*std::f64::consts::PI);
        ctx.fill().ok();
        Some(Value::from(&false))
}

pub fn draw_held_wire (
    v: &Viewport,
    ctx: &Context,
    from: Point2<i64>,
    to: Point2<i64>,
    flop: FlopDirection,
) -> Option<glib::Value> {
    draw_wire(v, ctx, from, to, flop).unwrap();
    Some(Value::from(&false))
}

pub fn draw_held_symbol(
    v: &Viewport,
    ctx: &Context,
    sym: &Symbol,
    x: f64,
    y: f64,
) -> Option<glib::Value> {
    ctx.save().unwrap();

    let coords = v.iform(Point2::new(x, y));
    let offset = v.xform(&coords);

    ctx.translate(offset.x, offset.y);

    draw_symbol(v, ctx, sym, None).unwrap();

    ctx.restore().unwrap();
    Some(Value::from(&false))
}

pub fn redraw_preview(v: &Viewport, ctx: &Context, sym: &Symbol) -> Option<glib::Value> {
    ctx.save().unwrap();
    let (width, height): (f64, f64) = v.dimensions();

    ctx.translate(width / 2.0, height / 2.0);
    //ctx.scale(1.0/2.0, 1.0/2.0);

    draw_symbol(v, ctx, sym, None).unwrap();

    ctx.restore().unwrap();
    Some(Value::from(&false))
}

/// Draws a series of wires, using nice joins as necessary.
fn draw_wire_smart<'a, I: Iterator<Item=(&'a Handle, &'a Wire)>>(v: &Viewport, 
                                                       ctx: &Context, 
                                                       sheet: &Sheet,
                                                       iter: I) -> Result<(), Error> {
    ctx.set_source_rgb(0.1, 0.5, 0.1);
    ctx.set_line_width(4.0);
    // We need to track where the last wire's far point was. This lets us know when
    // to insert a move_to and start a new path.
    let mut oldfar = None;
    for (handle, wire) in iter {
        let to = v.xform_o(wire.far().loc(sheet));
        let from = v.xform_o(handle.loc(sheet));
        match oldfar {
            None => {
                ctx.new_path();
                ctx.move_to(from.x, from.y);
            },
            Some(old) => {
                if !ptr::eq(old, handle) {
                    ctx.stroke()?;
                    ctx.new_path();
                    ctx.move_to(from.x, from.y);
                }// if old is handle, then we can just keep going
            }
        }
        match wire.flop() {
            FlopDirection::Undecided => {
                ctx.line_to(to.x, to.y);
            },
            FlopDirection::XThenY => {
                ctx.line_to(to.x, from.y);
                ctx.line_to(to.x, to.y);
            },
            FlopDirection::YThenX => {
                ctx.line_to(from.x, to.y);
                ctx.line_to(to.x,   to.y);
            }
        }
        oldfar = Some(wire.far());
    }
    ctx.stroke()?;
    Ok(())
}

/// Draws a single wire
fn draw_wire(v: &Viewport, ctx: &Context, 
             wfrom: na::Point2<i64>, wto: na::Point2<i64>, flop: FlopDirection)
-> Result<(), Error> {
    // Ok, horizontal, vertical, or compound?
    let to = v.xform_o(wto);
    let from = v.xform_o(wfrom);
    ctx.set_source_rgb(0.1, 0.5, 0.1);
    ctx.set_line_width(4.0);
    ctx.new_path();
    ctx.move_to(to.x, to.y);
    if (wto.x == wfrom.x) || (wto.y == wfrom.y) {
        // wire across/up
        ctx.line_to(from.x, from.y);
    } else if flop == FlopDirection::XThenY {
        ctx.line_to(from.x, to.y);
        ctx.line_to(from.x, from.y);
    } else {
        ctx.line_to(to.x, from.y);
        ctx.line_to(from.x, from.y);
    }
    ctx.stroke()?;
    Ok(())
}

/// Draws a single symbol
fn draw_symbol(v: &Viewport, ctx: &Context, symbol: &Symbol, instance: Option<&SymbolLoc>) -> Result<(), Error> {
    ctx.save().unwrap();
    for graphic in symbol.drawing() {
        match graphic {
            Graphic::Path(cmds, style) => {
                let w_scale = v.scale(&50_000);
                ctx.set_line_width(*style.weight() as f64*w_scale);
                ctx.new_path();
                for cmd in cmds {
                    match cmd {
                        PathElement::Move(x) => {
                            let pt = v.scale_pair(x);
                            ctx.move_to(pt.x, pt.y);
                        },
                        PathElement::Line(x) => {
                            let pt = v.scale_pair(x);
                            ctx.line_to(pt.x, pt.y);
                        },
                        PathElement::Arc{
                            center, radius,
                            start, end
                        } => {
                            let pt = v.scale_pair(center);
                            let radius = *radius as i64;
                            let r = v.scale(&radius);
                            let tau = 2.0*std::f64::consts::PI;
                            ctx.arc(pt.x, pt.y, r, 
                                    (*start as f64)*tau/1.0e6,
                                    (*end   as f64)*tau/1.0e6,
                                    );
                        },
                    }
                }
                if *style.closed() {
                    ctx.close_path();
                }
                match (style.stroke(), style.fill()) {
                    (Some(x), Some(y)) => {
                        let c = v.color_lookup(x);
                        ctx.set_source_rgb(c.0, c.1, c.2);
                        ctx.stroke_preserve()?;
                        let c = v.color_lookup(y);
                        ctx.set_source_rgb(c.0, c.1, c.2);
                        ctx.fill()?;
                    }
                    (Some(x), None) => {
                        let c = v.color_lookup(x);
                        ctx.set_source_rgb(c.0, c.1, c.2);
                        ctx.stroke()?;
                    },
                    (None, Some(x)) => {
                        let c = v.color_lookup(x);
                        ctx.set_source_rgb(c.0, c.1, c.2);
                        ctx.fill()?;
                    },
                    (None, None) => {
                    },
                }
            },
            Graphic::Text{style, content, loc} => {
                // decode the content
                let text = content.decode(symbol, instance);
                let loc = v.scale_pair(loc);
                // Nominal text height (global scaling factor)
                let scale = v.scale(&60_000);
                text_at(ctx, &text, loc, style, scale)?;
            },
        }
    }
    ctx.set_source_rgb(0.0, 0.0, 0.0);
    ctx.restore().unwrap();
    Ok(())
}

/// Draws text at specified location and style
fn text_at(
    ctx: &Context,
    text: &str,
    loc: Vector2<f64>,
    style: &TextStyle,
    scale: f64,
) -> Result<(), Error> {
    ctx.set_font_size(style.size()*scale);
    let extents = ctx.text_extents(text)?;
    let (width, height) = (extents.x_advance, extents.height);
    ctx.move_to(loc.x, loc.y);
    match style.vertical() {
        JustifyUD::Above => {
            ctx.rel_move_to(0.0, height*1.1);
        },
        JustifyUD::Center => {
            ctx.rel_move_to(0.0, height/2.05);
        },
        JustifyUD::Below => {
            ctx.rel_move_to(0.0, -height*0.1);
        },
    }
    match style.horizontal() {
        JustifyRL::Right => {
            ctx.rel_move_to(-width, 0.0);
        },
        JustifyRL::Left => {
            // default is left
        },
        JustifyRL::Center => {
            ctx.rel_move_to(-width/2.0, 0.0);
        },
    }
    if style.bold() || style.italic() {
        let cfname = ctx
            .font_face()
            .toy_get_family()
            .expect("Couldn't get font face name");
        let slant = if style.italic() {
            FontSlant::Italic
        } else {
            FontSlant::Normal
        };
        let weight = if style.bold() {
            FontWeight::Bold
        } else {
            FontWeight::Normal
        };
        ctx.select_font_face(&cfname, slant, weight);
    }
    ctx.show_text(text)?;
    if style.overline() {
        let extents = ctx.text_extents(text)?;
        ctx.rel_move_to(0.0, -extents.height);
        ctx.rel_line_to(-extents.width, 0.0);
        ctx.stroke()?;
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::view::*;
    #[test]
    fn xform_inv_check() {
        // in nm
        let vals = [-2000, -200, -1, -1_000_000, 1_000_000, 1, 200, 2000];
        let mut coordss = vec![];
        for val in vals {
            for val2 in vals {
                coordss.push((val*10_000, val2*10_000));
            }
        }
        let mut v = Viewport::new();
        v.scale(&1);
        v.scale(&1);
        v.scale(&-1);
        v.scale_about_point(-1.0, (100.0, 100.0));
        for coords in coordss {
            let screenspace = v.xform_o(coords);
            v.scale_about_point(-1.0, screenspace);
            let out = v.iform(screenspace);
            dbg!(coords, out);
            // 85_000 is default application pixel size
            assert!((coords.0 - out.0).abs() < 85_000/2);
            assert!((coords.1 - out.1).abs() < 85_000/2);
        }
    }
    #[test]
    fn xform_assumption_check_nonorigin() {
        // A coordinate at 10_000x pix_width, pix_height should go to 10000, 10000 right?
        let mag = 10_000;
        let coords = (mag*85_000, mag*85_000);
        let mag = mag as f64;
        let mut v = Viewport::new();
        let out = v.xform_o(coords);
        assert!((out.0 - mag).abs() < 1.0);
        assert!((out.1 - mag).abs() < 1.0);
        // The whole point of a scale_about_point function is that the point doesn't move...
        v.scale_about_point(4.0, (10000.0, mag));
        let out = v.xform_o(coords);
        assert!((out.0 - mag).abs() < 1.0);
        assert!((out.1 - mag).abs() < 1.0);
    }
    #[test]
    fn xform_scale() {
        let mut v = Viewport::new();
        // ps = page space, which are integer nanometer coords
        // ss = screen space, which are floating-point pixel coords
        let vals = [-2000, -200, -1, -1_000_000, 1_000_000, 1, 200, 2000];
        let mut coords_ps_orig = vec![];
        for val in vals {
            for val2 in vals {
                coords_ps_orig.push((val*10_000, val2*10_000));
            }
        }
        // If you round-trip them to floats, they WILL change, but
        // it should be idempotent
        let mut coords_ps = vec![]; 
        // And the state-space version should be stable as well
        let mut coords_ss = vec![];
        for coord in &coords_ps_orig {
            let ss = v.xform_o(*coord);
            let ps = v.iform(ss);
            coords_ps.push(ps);
            let ss_new = v.xform_o(ps);
            assert_eq!(ss, ss_new, "State-space output should be stable");
            coords_ss.push(ss_new);
        }
        // Assumption 1: transform a point, it transforms
        // back?
        for (ps, ss) in std::iter::zip(&coords_ps, coords_ss) {
            println!("Screen-space coords: {:?}", ss);
            assert_eq!(*ps, v.iform(ss), 
                       "double-round-tripped nm coordinate are not stable!");
        }
        // Assumption 2: you scale the whole viewport, the apparent distance between points
        // scales by same amount?
        let pairs: Vec<(Point2<i64>, Point2<i64>)> = 
            coords_ps.iter().map(|a| coords_ps.iter().map(move |b| (*a, *b)))
            .flatten().collect();
        // ratio x/y of distance vector between each pair of points
        let mut ratios = vec![];
        let mut aberation_max = 0.0;
        let mut aber_loc = None;
        let aberation_limit = 1e-4;
        for (i, (a, b)) in pairs.iter().enumerate() {
            let ax = v.xform_o(*a);
            let bx = v.xform_o(*b);
            let r = (ax.0 - bx.0)/(ax.1 - bx.1);
            ratios.push(r);
            println!("Pair {}:\t {:?}x{:?} have ratio {}",
                     i, a, b, r);
        }
        v.scale_about_point(5.0, (100.0, 100.0));
        // Are the ratios invariant?
        for (i, (a, b)) in pairs.iter().enumerate() {
            let ax = v.xform_o(*a);
            let bx = v.xform_o(*b);
            let r = (ax.0 - bx.0)/(ax.1 - bx.1);
            if !(f64::is_finite(r) && f64::is_finite(ratios[i])) {
                continue;
            }
            println!("Pair {}:\t {:?}x{:?} have ratio {} (prev: {})",
                     i, a, b, r, ratios[i]);
            let aberation = (r - ratios[i]).abs();
            if aberation > aberation_max {
                aber_loc = Some((i, a, b, ax, bx, 'a', aberation));
                aberation_max = aberation;
            }
        }
        v.scale_about_point(-2.0, (-600.0, 400.0));
        for (i, (a, b)) in pairs.iter().enumerate() {
            let ax = v.xform_o(*a);
            let bx = v.xform_o(*b);
            let r = (ax.0 - bx.0)/(ax.1 - bx.1);
            if !(f64::is_finite(r) && f64::is_finite(ratios[i])) {
                continue;
            }
            println!("Pair {}:\t {:?}x{:?} have ratio {} (prev: {})",
                     i, a, b, r, ratios[i]);
            let aberation = (r - ratios[i]).abs();
            if aberation > aberation_max {
                aber_loc = Some((i, a, b, ax, bx, 'b', aberation));
                aberation_max = aberation;
            }
        }
        v.scale_about_point(-1.0, (600.0, -300.0));
        for (i, (a, b)) in pairs.iter().enumerate() {
            let ax = v.xform_o(*a);
            let bx = v.xform_o(*b);
            let r = (ax.0 - bx.0)/(ax.1 - bx.1);
            if !(f64::is_finite(r) && f64::is_finite(ratios[i])) {
                continue;
            }
            println!("Pair {}:\t {:?}x{:?} have ratio {} (prev: {})",
                     i, a, b, r, ratios[i]);
            let aberation = (r - ratios[i]).abs();
            if aberation > aberation_max {
                aber_loc = Some((i, a, b, ax, bx, 'c', aberation));
                aberation_max = aberation;
            }
        }
        if let Some(aber_loc) = aber_loc {
            println!("Maximum aberation\ni:\t{}\nround:\t{}\naber:\t{}\
                     \na(x):\t{:?}\t{:?}\nb(x):\t{:?}\t{:?}",
                aber_loc.0, aber_loc.5, aber_loc.6, aber_loc.1, aber_loc.3, 
                aber_loc.2, aber_loc.4);
        }
        assert!(aberation_max < aberation_limit);
    }
}

