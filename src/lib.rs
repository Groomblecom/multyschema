pub mod model;
pub mod legacy_symbol;
pub mod symbol;
#[cfg(feature = "rendering")]
pub mod view;
pub mod storage;
pub mod default_library;

/// Shows an error or status message to the user, at a global
/// level.
fn post_to_user(message: UserMessage) {
    eprintln!("ATTN:\t{}", message.to_string());
}

#[derive(Debug, Clone)]
/// Messages to show through a top-level non-diagnoistic communication channel
pub enum UserMessage {
    UiReady,
    ConnectLoop,
    ZeroLength,
    BadLibrary(String),
    BadConversion(String),
}

// Please consider these strings placeholders! Eventually these
// will be sent through glib internationalization.
impl UserMessage {
    pub fn to_string(&self) -> String {
        match self {
            Self::UiReady => {
                "Multyschema ready!".to_string()
            },
            Self::ConnectLoop => {
                "You cannot connect a loop of wires, because it leads to messy
                diagrams and also it breaks the datastructure this program uses
                for storing them. Nice try!".to_string()
            },
            Self::ZeroLength => {
                "You cannot make a zero-length wire!".to_string()
            },
            Self::BadLibrary(s) => {
                format!("Library was malformed and did not load: {}", s)
            },
            Self::BadConversion(s) => {
                format!("While converting, skipped unparseable shape:\n\t {}",
                        s)
            },
        }
    }
}
