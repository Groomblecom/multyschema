//! Command-line interface for multyschema
//!
//! Provides file conversion, validation, and rendering
//! commands suitable for headless environments.

use clap::{Parser, Subcommand, Args};

use crate::storage::*;
use crate::view::*;

use cairo::{Context, PdfSurface};

use std::path::PathBuf;

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    #[clap(subcommand)]
    command: Option<Command>,
}

#[derive(Subcommand)]
enum Command {
    /// Convert binary and json .msy files
    Convert(Convert),
    /// Render a .msy file to a PDF of the schematic
    Render(Render),
}

#[derive(Args)]
struct Convert {
    /// Input file path
    infile: PathBuf,
    /// Output file path
    outfile: PathBuf,
    #[clap(short='B', long)]
    /// Is the input file binary?
    in_binary: bool,
    #[clap(short='b', long)]
    /// Should the output file be binary?
    out_binary: bool,
}

#[derive(Args)]
struct Render {
    /// Input file path
    infile: PathBuf,
    /// Path to render PDF to
    pdfpath: PathBuf,
    #[clap(short='B', long)]
    /// Is the input file binary?
    in_binary: bool,
}

pub fn run() {
    let args = Cli::parse();
    let command = if let Some(command) = args.command {
        command
    } else {
        return;
    };
    match command {
        Command::Convert(Convert{infile, in_binary, outfile, out_binary}) => {
            let file_o = if in_binary {
                load_binary_file(infile)
            } else {
                load_json(infile)
            }.unwrap();
            let mut file = vec![];
            for sheet in &file_o {
                file.push(sheet);
            }
            println!("Output binary: {}", out_binary);
            if out_binary {
                save_binary_file(outfile, file)
            } else {
                save_json(outfile, file)
            }.unwrap();
        },
        Command::Render(Render{infile, in_binary, pdfpath}) => {
            println!("Rendering file {:?}", infile);
            let mut file = if in_binary {
                load_binary_file(infile)
            } else {
                load_json(infile)
            }.unwrap();
            let sheet = file.pop().expect("Cannot render empty file!");
            // (1920px / 530mm) * (1 inch / 25.4mm) * ( 72 points / 1 inch )
            let pts_per_px = 1920.0 / 530.0 / 25.4 * 72.0;
            let surface = 
                PdfSurface::new(1920.0/pts_per_px, 1080.0/pts_per_px, pdfpath)
                .unwrap();
            let ctx = Context::new(&surface).unwrap();
            let v = Viewport::new();
            redraw_schematic(&v, &ctx, &sheet);
            surface.finish();
        },
    }
}
