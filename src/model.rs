
use std::collections::{HashMap, HashSet};
use std::sync::Arc;
use std::{fmt, iter, mem, ptr, slice};
use std::fmt::Formatter;
use std::sync::atomic::{AtomicU64, Ordering};

use gcd::Gcd;

use serde::{Serialize, Deserialize};

use crate::{post_to_user, UserMessage};
use crate::symbol::Symbol;

use nalgebra as na;
use na::Point2;

static HANDLE_NAME_COUNTER: AtomicU64 = AtomicU64::new(1);

#[derive(Debug)]
/// represents a single layer of interconnected symbols
pub struct Sheet {
    /// Current local symbol table
    symbol_table: HashMap<String, Arc<Symbol>>,
    /// Instantiations of symbols
    symbols: Vec<SymbolLoc>,
    /// List of circuit-theoretic nodes
    ///
    /// These own wires and handles that actually have
    /// positions on the schematic.
    nodes: Vec<Node>,
    /// Current top node ID for assigning unique IDs to them
    next_node_id: u64,
    /// Current top symbol ID
    next_sym_id: u64,
}

#[derive(Debug)]
/// Holds an instantiation of a symbol,
///
/// Includes positions, overrides of field variables, and
/// so on
pub struct SymbolLoc {
    parent: Arc<Symbol>,
    // TODO: De Morgan and alts indexing
    /// Position in sheet coordinates of origin of symbol
    loc: Point2<i64>,
    /// Unique ID number
    id: u64,
    /// Mangled parent symbol name
    mname: String,
    /// Component value
    /// 
    /// TODO: make it not a dummy value
    value: Option<f64>
}

impl Sheet {
    pub fn empty_default() -> Sheet {
        Sheet {
            symbol_table: HashMap::new(),
            nodes: vec![],
            symbols: vec![],
            next_node_id: 0,
            next_sym_id: 0,
        }
    }
    pub fn insert_symbol(&mut self, s: &Symbol, mname: &str, coords: Point2<i64>) {
        // Have we seen this symbol before?
        let arc = match self.symbol_table.get(mname) {
            Some(x) => {
                assert!(**x == *s, "Duplicate mangled names, different symbols!");
                x.clone()
            },
            None => {
                Arc::new(s.clone())
            },
        };
        let value = arc.default_value();
        self.symbol_table.insert(mname.to_string(), arc.clone());
        let id = self.next_sym_id;
        self.next_sym_id += 1;
        // Make it Nodes and Handles!
        let (mut gcdx, mut gcdy) = 
            (coords.x.abs() as u64, coords.y.abs() as u64);
        for terminal in s.terminals() {
            let offset = *terminal.offset();
            gcdx = gcdx.gcd((offset.x+coords.x).abs() as u64);
            gcdy = gcdy.gcd((offset.y+coords.y).abs() as u64);
            let disname = if terminal.name().len() > 0 {
                format!("U{}{}", id, terminal.name())
            } else {
                format!("U{}T{}", id, terminal.enumerated_id())
            };
            let h = Handle {
                disname: Some(disname),
                typ: HandleType::Terminal {
                    parent_id: id,
                    parent_index: self.symbols.len(),
                    terminal_enid: *terminal.enumerated_id(),
                    offset,
                },
                children: vec![],
            };
            self.insert_node(h);
        }
        println!("Necessary grid division: {}, {}", gcdx, gcdy);
        self.symbols.push(SymbolLoc {
            parent: arc,
            loc: coords,
            id,
            mname: mname.to_string(),
            value,
        });
    }
    pub fn symbols(&self) -> impl Iterator<Item = &SymbolLoc> {
        self.symbols.iter()
    }
    pub fn traverse(&self) -> impl Iterator<Item = (&Node, &Handle, &Wire)> {
        self.nodes.iter().flat_map(|n| n.traverse())
    }
    pub fn nodes(&self) -> impl Iterator<Item=&Node> {
        self.nodes.iter()
    }
    fn get_wires_under(&self, pt: Point2<i64>) -> Vec<(usize, WirePath)> {
        let mut ptlist = vec![];
        for (i, node) in self.nodes.iter().enumerate() {
            // Did we hit the root node with the first point?
            if pt == node.root.loc(self) {
                ptlist.push((i, node.root_path()));
            } else {
                let mut iter = node.traverse();
                for (_node, handle, wire) in &mut iter {
                    // Did we hit the wire's far point with the first point?
                    if pt == wire.far.loc(self) {
                        ptlist.push((i, iter.path(false)));
                        break;
                    } else {
                        // did we hit the wire with the first point?
                        if hit_test_wire(pt, handle.loc(self), wire.far.loc(self), wire.flop) {
                            ptlist.push((i, iter.path(true)));
                            break;
                        }
                    }
                }
            }
        }
        ptlist
    }
    fn insert_node(&mut self, root: Handle) {
        let id = self.next_node_id;
        self.next_node_id += 1;
        self.nodes.push(Node {
            root,
            id,
            generation: 0,
        });
    }
    pub fn insert_wire(&mut self, from: Point2<i64>, to:Point2<i64>, flop: FlopDirection) -> bool {
        // You can't insert a zero-length wire -- nice try!
        if from == to {
            post_to_user(UserMessage::ZeroLength);
            return false;
        }
        // Undecided wires MUST be purely linear.
        assert!(flop != FlopDirection::Undecided || (from.x == to.x || from.y == to.y));
        // DEBUG
        println!("Before-state: {:-#?}", self.nodes);
        // Scan for connections to other wires:
        let from_h = self.get_wires_under(from).pop();
        let to_h = self.get_wires_under(to).pop();
        println!("from_h: {:?}\tto_h: {:?}", from_h, to_h);
        let done = to_h.is_some();
        match ((from_h, from), (to_h, to)) {
            // Neither end was connected
            ((None, _), (None, _)) => {
                let far = Handle {
                        disname: handle_disname("LF"),
                        typ: HandleType::Fixed(to),
                        children: vec![],
                    };
                let wire = Wire {
                    far,
                    flop,
                };
                let root = Handle {
                    disname: handle_disname("LR"),
                    typ: HandleType::Fixed(from),
                    children: vec![wire],
                };
                self.insert_node(root);
            },
            // One end hit
            ((Some(x), near), (None, far)) | ((None, far), (Some(x), near)) => {
                let (i, path) = x;
                // Make a new handle at the far end
                let far_h = Handle {
                        disname: handle_disname("OL"),
                        typ: HandleType::Fixed(far),
                        children: vec![],
                    };
                let wire = Wire {
                    far: far_h,
                    flop,
                };
                // Attach our wire into the tree, splitting a wire to get a handle
                // if necessary
                match self.nodes[i].get_wire_or_handle_mut(path) {
                    WireOrHandle::Wire(w) => {
                        w.split(Handle {
                            disname: handle_disname("OH"),
                            typ: HandleType::Fixed(near),
                            children: vec![wire],
                        });
                    },
                    WireOrHandle::Handle(h) => {
                        h.children.push(wire);
                    },
                }
            },
            // They hit both
            ((Some((a_i, a)), a_l), (Some((b_i, b)), b_l)) => {
                // You can't connect a loop of wires!
                if a_i == b_i {
                    post_to_user(UserMessage::ConnectLoop);
                    return false;
                }
                // It really doesn't matter which we pick to
                // re-root, so we're picking the one with higher index.
                let (a, a_i, a_l, b, b_i, b_l) = if a_i > b_i {
                    (a, a_i, a_l, b, b_i, b_l)
                } else {
                    (b, b_i, b_l, a, a_i, a_l)
                };
                // which is now called a.
                println!("Rerooting at path {:?}", a);
                self.nodes[a_i].reroot_about(&self.symbols, a, a_l);
                // The node we want is now the root of A, handily
                let an = self.nodes.remove(a_i);
                println!("Concatting at path {:?}", b);
                self.nodes[b_i].concat(b, b_l, flop, an);
            }
        }
        // DEBUG
        println!("After-state: {:-#?}", self.nodes);
        done
    }
}

/// True iff the wire is under the point given
///
/// The wire is defined by from, to, and flop.
fn hit_test_wire(point: Point2<i64>, from: Point2<i64>, to: Point2<i64>, flop: FlopDirection)
    -> bool {
        let tol = 2_000_000;//DEBUG
        let (x, y) = (point.x, point.y);
        // did we hit it?
        match flop {
            FlopDirection::XThenY => {
                if from.y == y && contained(x, from.x, to.x)  {
                    // hit on first segment
                    true
                } else if to.x == x && contained(y, from.y, to.y) {
                    // hit on second segment
                    true
                } else {
                    // DEBUG
                    if i64::abs(from.y - y) < tol && contained(x, from.x, to.x) {
                        println!("Near miss, {:?}\t{:?}", from, to);
                    } else if i64::abs(to.x - x) < tol && contained(y, from.y, to.y) {
                        println!("Near miss, {:?}\t{:?}", from, to);
                    }// DEBUG
                    false
                }
            },
            FlopDirection::YThenX => {
                if from.x == x && contained(y, from.y, to.y)  {
                    // hit on first segment
                    true
                } else if to.y == y && contained(x, from.x, to.x) {
                    // hit on second segment
                    true
                } else {
                    // DEBUG
                    if i64::abs(to.y - y) < tol && contained(x, from.x, to.x) {
                        println!("Near miss, {:?}\t{:?}", from, to);
                    } else if i64::abs(from.x - x) < tol && contained(y, from.y, to.y) {
                        println!("Near miss, {:?}\t{:?}", from, to);
                    }// DEBUG
                    false
                }
            },
            FlopDirection::Undecided => {
                if from.x == x && contained(y, from.y, to.y)  {
                    // hit on vertical
                    true
                } else if from.y == y && contained(x, from.x, to.x) {
                    // hit on horizontal
                    true
                } else {
                    false
                }
            }
        }
    }

/// Helper function to determine if
/// a value y is in a range, y0 < y < y1
/// Order of limits unimportant
fn contained(y: i64, y0: i64, y1: i64) -> bool {
    let lower = i64::min(y0, y1);
    let upper = i64::max(y0, y1);
    lower <= y && y <= upper
}

impl SymbolLoc {
    pub fn coords(&self) -> Point2<i64> {
        self.loc
    }
    pub fn symbol(&self) -> &Symbol {
        &*self.parent
    }
    /// returns the value of this instance of the symbol
    pub fn value(&self) -> Option<f64> {
        self.value
    }
}

#[derive(Debug, PartialEq, Copy, Clone, Serialize, Deserialize)]
/// Whether a wire passes though the highest corner not
/// an endpoint or the lowest
///
/// A wire can go up, then over; or over, then up. This
/// controls which. Alternately, if you flop a wire
/// over the diagonal, that's swapping between the two variants here.
pub enum FlopDirection {
    XThenY,
    YThenX,
    Undecided,
}

impl FlopDirection {
    fn flip(&self) -> FlopDirection {
        match self {
            Self::XThenY => Self::YThenX,
            Self::YThenX => Self::XThenY,
            Self::Undecided => Self::Undecided,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
/// A schematic wire.
///
/// Schematic wires connect exactly two handles. Handles
/// and wires form a tree datastructure, where wires are edges
/// and handles are (little-n, datastructure-sense) nodes. Each
/// (big-N, electrical-sense) Node owns such a structure.
pub struct Wire {
    far: Handle,
    flop: FlopDirection,
}

impl Wire {
    pub fn far(&self) -> &Handle {
        &self.far
    }
    pub fn flop(&self) -> FlopDirection {
        self.flop
    }
    pub fn new(far: Handle, flop: FlopDirection) -> Self {
        Wire {far, flop}
    }
    pub fn split(&mut self, mut at: Handle) {
        mem::swap(&mut self.far, &mut at);
        self.far.children.push(Wire::new(at, self.flop));
    }
    fn newick(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        write!(f, "(")?;
        self.far.newick(f)?;
        write!(f, ")")?;
        Ok(())
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Handle {
    typ: HandleType,
    disname: Option<String>,
    /// Wires pointing to handles downstream (in arbitrarily-ordered
    /// per-node handle-wire tree).
    children: Vec<Wire>,
}

impl Handle {
    pub fn loc(&self, sheet: &Sheet) -> Point2<i64> {
        self.loc_list(&sheet.symbols)
    }
    /// Finds the location using a list of symbols rather than the whole
    /// sheet
    ///
    /// Used to avoid an aliasing issue in reroot_about
    fn loc_list(&self, list: &Vec<SymbolLoc>) -> Point2<i64> {
        match self.typ {
            HandleType::Fixed(loc) => loc,
            HandleType::Terminal{parent_id, parent_index, offset,..} => {
                let parent = &list[parent_index];
                assert_eq!(parent.id, parent_id,
                           "Symbol index immutability invariant violated!");
                parent.loc + offset
            },
        }
    }
    pub fn num_children(&self) -> usize {
        self.children.len()
    }
    fn dot(&self, f: &mut Formatter<'_>, counter: &mut u64, name: u64)
        -> Result<(), fmt::Error> {
            for child in &self.children {
                *counter += 1;
                let child_id = *counter;
                match (self.disname.as_ref(), child.far.disname.as_ref()) {
                    (None, None) => writeln!(f, "A{} -- A{};", name, child_id)?,
                    (Some(a), None) => writeln!(f, "{} -- A{};", a, child_id)?,
                    (None, Some(b)) => writeln!(f, "A{} -- {};", name, b)?,
                    (Some(a), Some(b)) => writeln!(f, "{} -- {};", a, b)?,
                }
                child.far.dot(f, counter, child_id)?;
            }
            Ok(())
    }
    fn newick(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        write!(f, "(")?;
        let mut iter = self.children.iter().peekable();
        while let Some(wire) = iter.next() {
            wire.newick(f)?;
            if !iter.peek().is_none() {
                write!(f, ",")?;
            }
        }
        if let Some(name) = &self.disname {
            write!(f, "){}", name)?;
        } else {
            write!(f, ")")?;
        }
        if let HandleType::Fixed(_) = &self.typ {
            write!(f, "F")?;
        } else {
            write!(f, "T")?;
        }
        Ok(())
    }
    /// Tests if this handle is for connecting to a symbol
    pub fn is_terminal(&self) -> bool {
        if let HandleType::Terminal {..} = self.typ {
            true
        } else {
            false
        }
    }
    /// Gets the handle type
    pub fn typ(&self) -> &HandleType {
        &self.typ
    }
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub enum HandleType {
    Terminal {
        parent_id: u64,
        parent_index: usize,
        offset: na::Vector2<i64>,
        terminal_enid: u64,
    },
    Fixed(Point2<i64>),
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Node {
    /// Arbitrary root of wire-handle tree.
    root: Handle,
    /// Globally unique internal ID of this Node, used so that we can
    /// later associate terminals to it
    id: u64,
    /// Generation counter, so we can catch stale paths into the
    /// wire-handle tree
    generation: u64,
}

impl fmt::Debug for Node {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        if f.alternate() {
            if f.sign_minus() {
                let mut counter = 1;
                writeln!(f, "graph {{")?;
                self.root.dot(f, &mut counter, 0)?;
                writeln!(f, "}}")
            } else {
                self.newick(f)
            }
        } else {
            f.debug_struct("Node")
                .field("root", &self.root)
                .field("id", &self.id)
                .field("generation", &self.generation)
                .finish()?;
            write!(f, "\n")
        }
    }
}

#[derive(Debug)]
enum WireIteratorChoice {
    Root,
    Twig(usize),
    MaybeLeaf,
}

#[derive(Debug)]
pub struct WireIterator<'a> {
    node: &'a Node,
    parent_iter: Option<Box<WireIterator<'a>>>,
    curr: &'a Handle,
    handle_iter: iter::Enumerate<slice::Iter<'a, Wire>>,
    // Used for computing path vectors. Represents the current
    // iterator index in the handle_iter.
    choice: WireIteratorChoice,
}
impl<'a> Iterator for WireIterator<'a> {
    type Item = (&'a Node, &'a Handle, &'a Wire);

    fn next(&mut self) -> Option<Self::Item> {
        let windex = self.handle_iter.next();
        match windex {
            Some((index, wire)) => {
                // Pulled a wire out, move down the tree
                let ret = Some((self.node, self.curr, wire));
                self.choice = WireIteratorChoice::Twig(index);
                // We are making a new iterator, then putting the current
                // one in it's parent_iter field. This swap sets up
                // the future one here as child_iter, swaps it into self
                // so that child_iter is now the old self, then puts
                // a pointer to the old self in the current self's parent
                // field. It's a little strange looking but it does work
                // and it satisfies the borrow checker!
                let mut child_iter = WireIterator {
                    node: self.node,
                    parent_iter: None,
                    curr: &wire.far,
                    handle_iter: wire.far.children.iter().enumerate(),
                    choice: WireIteratorChoice::MaybeLeaf,
                };
                mem::swap(&mut child_iter, self);
                self.parent_iter = Some(Box::new(child_iter));
                ret
            },
            None => {
                // Run out of nodes in current handle, jump up a level
                match self.parent_iter.take() {
                    Some(parent) => {
                        *self = *parent;
                        self.next()
                    },
                    // If you run out of levels to jump, you're done
                    None => {
                        self.choice = WireIteratorChoice::Root;
                        None
                    }
                }
            }
        }
    }
}

impl WireIterator<'_> {
    /// Get a path specifier for the current wire/handle the
    /// node is pointed to.
    ///
    /// This is because writing mutable tree iterators is (a) hard
    /// and (b) full of foot-guns, so instead, this immutable tree iterator
    /// lets you walk all around to find interesting nodes, and then gives
    /// you handles to look them up again later, to mutate them!
    ///
    /// These become stale once you mutate the tree with them, and
    /// stop working! They use a generation system, so once you use one,
    /// they all break until you go generate more.
    pub fn path(&self, to_wire: bool) -> WirePath {
        let mut indices = vec![];
        // little-n node
        let mut c_node: &Self = self;
        loop {
            match c_node.choice {
                WireIteratorChoice::Root => break,
                WireIteratorChoice::MaybeLeaf => {
                    c_node = & *c_node.parent_iter.as_ref().unwrap();
                },
                WireIteratorChoice::Twig(index) => {
                    indices.push(index);
                    match &c_node.parent_iter {
                        Some(x) => {
                            c_node = & *x;
                        },
                        None => break,
                    }
                }
            }
        }
        WirePath {
            id: self.node.id,
            generation: self.node.generation,
            indices,
            is_wire: to_wire,
        }
    }
}

impl Node {
    pub fn traverse(&self) -> WireIterator {
        WireIterator {
            node: self,
            parent_iter: None,
            curr: &self.root,
            handle_iter: self.root.children.iter().enumerate(),
            choice: WireIteratorChoice::Root,
        }
    }
    /// Iterates over every handle in this node, including the root
    pub fn handles(&self) -> impl Iterator<Item=&Handle> {
            iter::once(&self.root)
                .chain(self.traverse().map(|x| x.2.far()))
    }
    /// Returns false if the guard was violated.
    fn wire_path_guard(&mut self, path: &WirePath) -> bool {
        // Right Node?
        assert_eq!(path.id, self.id,
                   "You cannot use a WirePath with a different node than it was\
                   constructed from!");
        if self.generation != path.generation {
            eprintln!("Tried to use a generation {} WirePath with Node {}, currently gen {}", 
                      path.generation, self.id, self.generation);
            return false;
        }
        // Update generation counter!
        self.generation += 1;
        true
    }
    /// Get a handle at the specified coordinates, at the specifed
    /// tree location, creating it if necessary.
    ///
    /// Will panic if the path guard fails.
    fn get_wire_or_handle_mut<'a>(&'a mut self, mut path: WirePath) -> WireOrHandle<'a> {
        if !self.wire_path_guard(&path) {
            panic!("Bad wire path!")
        }
        println!("Passed wire path guard!");
        path.indices.reverse();
        let mut iter = path.indices.into_iter().peekable();
        // walk forward until we get to the tree location
        // MAKE peekable?
        //
        let mut curr = WireOrHandle::Handle(&mut self.root);
        while let Some(index) = iter.next() {
            // index is from current node -> wire,
            // peek_index is from next node -> next wire
            // We are at the end if there is no peek_index
            curr = curr.advance_handle(index);
            if iter.peek().is_none() {
                if path.is_wire {
                    return curr;
                } else {
                    curr = curr.advance_wire();
                    return curr;
                }
            } else {
                curr = curr.advance_wire();
            }
        }
        return curr;
    }
    /// Gets a path to the root node.
    pub fn root_path(&self) -> WirePath {
        WirePath {
            id: self.id,
            generation: self.generation,
            indices: vec![],
            is_wire: false,
        }
    }
    /// Returns true iff the passed handle is the root
    /// of this Node's tree
    ///
    /// Used to determine total number of connections to a handle
    /// when deciding to render a junction dot.
    pub fn is_root(&self, h: &Handle) -> bool {
        ptr::eq(h, &self.root)
    }
    /// Finds or makes a node at the coordinates, then makes it the root
    ///
    /// If this returned false, the path was bad!
    /// The symbols vector passed here is to avoid borrow aliasing!
    fn reroot_about(
        &mut self, symbols: &Vec<SymbolLoc>,
        mut path: WirePath,
        loc: Point2<i64>) -> bool {
        if !self.wire_path_guard(&path) {
            return false;
        }
        println!("Before reroot: {:-#?}", self);
        // we just flip the direction of every link on our way down the
        // tree -- so, pop off the child, make the parent it's child, then repeat
        path.indices.reverse();
        let mut iter = path.indices.into_iter().peekable();
        while let Some(index) = iter.next() {
            // Is this the ultimate index, making a path to a wire?
            if iter.peek().is_none() && path.is_wire {
                // Split the wire in two, then put both into a new root's
                // children
                //
                let Wire {far: child, flop} = self.root.children.remove(index);
                // child is too far
                // current root is too near
                // make a new root
                let mut spare = Handle {
                    disname: handle_disname("NR"),
                    typ: HandleType::Fixed(loc),
                    children: vec![],
                };
                mem::swap(&mut spare, &mut self.root);
                // spare is now the too near one
                self.root.children.push(Wire {
                    far: spare,
                    flop,
                });
                self.root.children.push(Wire {
                    far: child,
                    flop,
                });
            } else {
                // just flip wires as we go down the tree, which will
                // ultimately put the correct node in the root.
                let Wire {far: mut child, flop} = self.root.children.remove(index);
                mem::swap(&mut child, &mut self.root);
                self.root.children.push(Wire {
                    far: child,
                    flop: flop.flip(),
                });
            }
        }
        assert_eq!(self.root.loc_list(symbols), loc, 
                   "Broken invariant: rerooting about a coordinate");
        println!("after reroot: {:-#?}", self);
        true
    }
    /// Puts the passed node as a wire off the passed path, making a handle if
    /// necessary at the location.
    ///
    /// Panics if path guard fails.
    fn concat(&mut self,
              path: WirePath,
              loc: Point2<i64>,
              flop: FlopDirection, 
              node: Node) {
        // rip out the old node's root and put it in a wire
        let cutwire = Wire {
            far: node.root,
            flop,
        };
        // find out where we're putting it
        match self.get_wire_or_handle_mut(path) {
            WireOrHandle::Wire(w) => {
                // split the wire
                w.split(Handle {
                    typ: HandleType::Fixed(loc),
                    children: vec![cutwire],
                    disname: handle_disname("SA"),
                });
            },
            WireOrHandle::Handle(h) => {
                // just attach directly
                h.children.push(cutwire);
            }
        };
    }
    /// Exports this Node's handle-wire tree in Newick graph notation
    ///
    /// This is suitable for visualization
    /// Use the pretty-print debug flag like so:
    /// println!("Newick of node: {:#?}", node);
    pub fn newick(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        self.root.newick(f)?;
        write!(f, ";")
    }
    /// Returns the root node of the Node
    pub fn root(&self) -> &Handle {
        &self.root
    }
}

#[derive(Debug)]
enum WireOrHandle<'a> {
    Wire(&'a mut Wire),
    Handle(&'a mut Handle),
}

impl<'a> WireOrHandle<'a> {
    /// advances a wire into its far handle
    fn advance_wire(self) -> Self {
        if let Self::Wire(w) = self {
            Self::Handle(&mut w.far)
        } else {
            panic!("Cannot advance_wire a non-wire WoH! You have a bug.");
        }
    }
    /// advances a handle into a child wire
    fn advance_handle(self, index: usize) -> Self {
        if let Self::Handle(h) = self {
            Self::Wire(&mut h.children[index])
        } else {
            panic!("Cannot advance_wire a non-wire WoH! You have a bug.");
        }
    }
}

/// Non-borrowing specifier for a given wire or handle
///
/// We want to be able to iterate over all the tree nodes, compare them, and
/// then potentially much later, mutate just one of them. To do this, we just
/// make a vector of child indices as we traverse the tree, then later we can go
/// get a nice mutable reference without having to re-traverse the tree. Since
/// the vector doesn't borrow the tree, to prevent errors, we keep a generation
/// counter on the node, and tag paths with a generation. The counter is
/// incremented every time the tree is mutated. Trying to follow a stale path
/// gives a None. Trying to use a path with a different node than it came
/// from panics.
#[derive(Debug)]
pub struct WirePath {
    /// Generation marker to prevent stale paths from
    /// being followed
    generation: u64,
    /// ID of node this path applies to
    id: u64,
    /// List of indicies in stack order.
    indices: Vec<usize>,
    /// Does this point to a Handle or a Wire?
    is_wire: bool,
}

fn handle_disname(tag: &'static str) -> Option<String> {
    Some(format!(
            "{}{}", 
            tag, 
            HANDLE_NAME_COUNTER.fetch_add(1, Ordering::SeqCst)
            ))
}

#[derive(Serialize, Deserialize, Debug)]
/// Represents a SymbolLoc ready for storage
/// inside a frozen sheet datastructure.
struct FrozenSymbolLoc {
    /// Position in sheet coordinates of origin of symbol
    loc: Point2<i64>,
    /// Unique ID number
    id: u64,
    /// Mangled parent symbol name
    mname: String,
    /// Value of this instance
    value: Option<f64>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct FrozenSheet {
    /// Current local symbol table
    symbol_table: HashMap<String, Symbol>,
    /// Instantiations of symbols
    symbols: Vec<FrozenSymbolLoc>,
    /// List of circuit-theoretic node and their wires
    nodes: Vec<Node>,
}

#[derive(Debug)]
pub enum ThawError {
    ForbiddenValue(String),
    DuplicateLocId(u64),
    MissingLocIndex(usize),
    MissingMName(String),
    ForbiddenMName(String),
    DuplicateTerminal(u64, u64),
    BadLocId(usize, u64),
    DubiousDisplayParameters,
    BadTerminal(u64),
}

impl fmt::Display for ThawError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::ForbiddenValue(s) => {
                write!(f,
                      "A symbol had value {} (characters: {:?}). \
                      names must not contain NUL characters.",
                      s, s.as_bytes())
            },
            Self::DuplicateLocId(id) => {
                write!(f, "There were two SymbolLocs with id {}", id)
            },
            Self::MissingLocIndex(id) => {
                write!(f,
                       "A terminal handle referred to non-existent \
                       SymbolLoc index {}",
                       id) 
            },
            Self::MissingMName(s) => {
                write!(f,
                      "A SymbolLoc had a parent symbol {}, \
                      but no such symbol was found",
                      s)
            },
            Self::ForbiddenMName(s) => {
                write!(f,
                      "A symbol had mangled name {} (characters: {:?}). \
                      Mangled names must not contain NUL characters.",
                      s, s.as_bytes())
            },
            Self::DuplicateTerminal(id, teid) => {
                write!(f,
                      "Two different terminal handles for SymbolLoc {},\
                      terminal enumerated id {} exist!",
                      id, teid)
            },
            Self::BadLocId(index, id) => {
                write!(f,
                      "A terminal handle points to SymbolLoc index {}, which \
                      exists but doesn't have ID {}. Index invariant broken.",
                      index, id)
            },
            Self::DubiousDisplayParameters => {
                write!(f,
                      "Display parameters (font size, etc.) out of reasonable\
                      range! Your file is likely corrupt.")
            },
            Self::BadTerminal(i) => {
                write!(f, 
                      "A text graphic element referred to a nonexistant \
                      terminal at index {}", i)
            }
        }
    }
}

impl FrozenSheet {
    pub fn freeze(sheet: &Sheet) -> Self {
        let mut symbols = vec![];
        for s in &sheet.symbols {
            symbols.push(FrozenSymbolLoc {
                loc: s.loc,
                id: s.id,
                mname: s.mname.clone(),
                value: s.value,
            });
        }
        let mut symbol_table = HashMap::with_capacity(sheet.symbol_table.len());
        for (k, v) in &sheet.symbol_table {
            symbol_table.insert(k.clone(), (**v).clone());
        }
        // TODO: Maybe do index compaction/deletion on SymbolLocs?
        // Ditto Symbol
        FrozenSheet {
            symbols,
            nodes: sheet.nodes.clone(),
            symbol_table,
        }
    }
    pub fn thaw(mut self) -> Result<Sheet, ThawError> {
        let mut symbol_table = HashMap::with_capacity(self.symbol_table.len());
        for (k, v) in self.symbol_table.drain() {
            if let Some(val) = v.validate_strings()  {
                return Err(ThawError::ForbiddenValue(val));
            }
            if k.contains('\0') {
                return Err(ThawError::ForbiddenMName(k));
            }
            if let Some(x) = v.validate_term_indicies() {
                return Err(ThawError::BadTerminal(x));
            }
            symbol_table.insert(k, Arc::new(v));
        }
        let mut new_symbols = vec![];
        let mut max_id = 0;
        let mut ids = HashSet::new();
        for symbol in self.symbols {
            let parent = match symbol_table.get(&symbol.mname) {
                Some(x) => x.clone(),
                None => {
                    return Err(ThawError::MissingMName(symbol.mname));
                }
            };
            if !ids.insert(symbol.id) {
                return Err(ThawError::DuplicateLocId(symbol.id));
            }
            if max_id < symbol.id {
                max_id = symbol.id;
            }
            new_symbols.push(SymbolLoc {
                mname: symbol.mname,
                parent,
                loc: symbol.loc,
                id: symbol.id,
                value: symbol.value,
            });
        }
        // Nodes are basically OK but need validation
        let mut terminal_doubles = HashSet::new();
        let mut max_node_id = 0;
        let mut gcd: Option<u64> = None;
        for node in &self.nodes {
            if max_node_id < node.id {
                max_node_id = node.id;
            }
            validate_handle(&node.root, &new_symbols, &mut terminal_doubles,
                            &mut gcd)?;
            for (_node, _handle, wire) in node.traverse() {
                validate_handle(&wire.far, &new_symbols, &mut terminal_doubles,
                                &mut gcd)?;
            }
        }
        if let Some(gcd) = gcd {
            if gcd < 10000 || gcd > 5000000 {
                // TODO post a grid warning
                eprintln!("Grid gcd suspicious: {}", gcd);
            }
        }
        Ok(Sheet {
            symbol_table,
            symbols: new_symbols,
            nodes: self.nodes,
            next_node_id: max_node_id + 1,
            next_sym_id: max_id + 1,
        })
    }
}

fn validate_handle(h: &Handle, new_symbols: &Vec<SymbolLoc>, 
                   terminal_doubles: &mut HashSet<(u64, u64)>,
                   gcd: &mut Option<u64>)
    -> Result<(), ThawError> {
        match h.typ {
            HandleType::Fixed(_) => {},
            HandleType::Terminal 
            {parent_id, parent_index, terminal_enid, ..} => {
                if !terminal_doubles
                    .insert((parent_id, terminal_enid)) {
                        Err(ThawError::DuplicateTerminal(
                                parent_id, terminal_enid))?;
                }
                match new_symbols.get(parent_index) {
                    Some(x) => {
                        if x.id != parent_id {
                            Err(ThawError::BadLocId(
                                    parent_index, parent_id))?;
                        }
                        // it's a good handle, nothing needs to be done
                    },
                    None => {
                        Err(ThawError::MissingLocIndex(parent_index))?;
                    }
                }
            },
        }
        let loc = h.loc_list(new_symbols);
        let (x, y) = (loc.x.abs() as u64, loc.y.abs() as u64);
        match gcd {
            Some(g) => *gcd = Some(g.gcd(x).gcd(y)),
            None => *gcd = Some(x.gcd(y)),
        }
        Ok(())
}

#[cfg(test)]
mod tests {
    use crate::model::*;
    #[test]
    fn test_t() {
        let grid = 1250000;
        let mut s = Sheet::empty_default();
        // vertical of T
        s.insert_wire((0, grid*-4), (0, grid*-2), FlopDirection::Undecided);
        // horizontal of T
        s.insert_wire((-grid, 0), (grid, 0), FlopDirection::Undecided);
        // Complete the problem T
        s.insert_wire((0, grid*-2), (0, 0), FlopDirection::Undecided);
        // There must be only one node left!
        assert_eq!(s.nodes.len(), 1);
        // There must be a junction (3 meeting) handle somewhere!
        let mut worked = false;
        if s.nodes[0].root.children.len() > 2 {
            worked = true;
        }
        for (_, handle, _) in s.traverse() {
            if handle.children.len() > 1 {
                worked = true;
            }
        }
        assert!(worked);
    }
    #[test]
    fn test_z() {
        let grid = 1250000;
        let mut s = Sheet::empty_default();
        // horizontal of T
        s.insert_wire((-2*grid, 0), (grid*2, 0), FlopDirection::Undecided);
        // off-center vertical
        s.insert_wire((-1*grid, grid*-2), (-1*grid, 0), FlopDirection::Undecided);
        // off-center other vertical
        s.insert_wire((1*grid, grid*2), (1*grid, 0), FlopDirection::Undecided);
        // should have single node!
        assert_eq!(s.nodes.len(), 1);
    }
}
