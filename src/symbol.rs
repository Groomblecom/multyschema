//! Native symbol format
//!
//! A symbol in Multyschema is a little different than traditional ECAD
//! programs. A symbol in Multyschema is:
//!   - a drawing 
//!   - a collection of named special locations (henceforth terminals) for
//!     connecting other symbols
//!   - a representation of single, self-contained electronic device 
//!     (excepting "global state" like power supplies, contained connectors,
//!     microcontroller code, etc.)
//!   - potentially simulatable 
//! A symbol in Multyschema is NOT:
//!   - a particular orderable part number
//!   - necessarily a single physical part (could be a functional subunit of one)
//!   - a product of a particular manufacturer
//!   - 1-to-1 with a circuit board footprint
//!   - a collection of terminals named alike to the physical pins of a part

use nalgebra as na;
use derive_getters::Getters;
use serde::{Serialize, Deserialize};
use std::{cmp, hash};
use crate::{
    legacy_symbol::PinOrientation,
    storage::validate_string,
    model::SymbolLoc,
};

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
/// The smallest indivisble element of a schematic drawing.
pub struct Symbol {
    drawing: Vec<Graphic>,
    terminals: Vec<Terminal>,
    name: String,
    prefix: char,
    // TODO: make this a more general type
    default_value: Option<f64>,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum Graphic {
    Path(Vec<PathElement>, PathStyle),
    Text {
        style: TextStyle,
        content: TextEntry,
        loc: na::Vector2<i64>,
    }
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub struct TextStyle {
    stroke: Color,
    italic: bool,
    bold: bool,
    overline: bool,
    fontsize: u64,
    fontname: Option<String>,
    jv: JustifyUD,
    jh: JustifyRL,
}

#[derive(Debug, Clone, Deserialize, Serialize, Getters, PartialEq)]
pub struct PathStyle {
    stroke: Option<Color>,
    fill: Option<Color>,
    joins: Join,
    weight: u8,
    closed: bool,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum Join {
    Miter,
    Round,
    Bevel,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq, Copy)]
pub enum Color {
    Primary,
    Secondary,
    Tertiary,
    // maybe add a custom color variant? Not sure what you'd use it for.
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum TextEntry {
    Reference,
    Device,
    Terminal(u64),
    Value,
    Custom (String),
}

impl TextEntry {
    pub fn decode(&self, s: &Symbol, l: Option<&SymbolLoc>) -> String {
        use TextEntry::*;
        match (self, l) {
            (Reference, _) => format!("{}??", s.prefix),
            (Device, _) => s.name.clone(),
            (Terminal(id), _) => s.terminals[*id as usize].name.clone(),
            (Value, Some(l)) => match l.value() {
                Some(x) => format!("{}", x),
                None => "~".to_string()
            },
            (Value, None) => match s.default_value() {
                Some(x) => format!("{}", x),
                None => "~".to_string()
            },
            (Custom(s), _) => s.clone(),
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum PathElement {
    /// Clockwise arc about point
    Arc {
        /// Center coordinate
        center: na::Vector2<i64>,
        /// Radius
        radius: u64,
        /// Start angle, in millionths of a τ (i.e. 1e6 = full circle)
        start: u32, 
        /// End angle, millionths of a τ
        end: u32
    },
    /// (poly-)line segment, end coordinates
    Line(na::Vector2<i64>),
    /// Pen placement operation, coordinates
    Move(na::Vector2<i64>),
    // TODO: Bezier arc type
}


#[derive(Clone, Debug, Getters, Deserialize, Serialize, PartialOrd, Eq)]
/// Electrical-level information about a pin
///
/// Eq implementation intentionally does not check the ID.
pub struct Terminal {
    // does NOT map to what other people call pin number
    // because, among other things, this is guaranteed to be unique within
    // a Symbol.
    enumerated_id: u64,
    name: String,
    typ: PinType,
    offset: na::Vector2<i64>,
}

impl Terminal {
    pub fn new(name: String, typ: PinType, offset: na::Vector2<i64>,
               enumerated_id: u64) -> Self {
        Self {name, typ, offset, enumerated_id}
    }
}

impl PartialEq for Terminal {
    fn eq(&self, other: &Self) -> bool {
        *self.name == *other.name 
            && self.typ == other.typ
            && self.offset == other.offset
    }
}

impl Ord for Terminal {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        self.name.cmp(&other.name)
            .then(self.typ.cmp(&other.typ))
            .then(self.offset.x.cmp(&other.offset.x))
            .then(self.offset.y.cmp(&other.offset.y))
    }
}

impl hash::Hash for Terminal {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        self.name.hash(state);
        self.typ.hash(state);
        self.offset.hash(state);
    }
}

#[derive(Copy, Clone, Debug, Deserialize, Serialize,
         PartialEq, Hash, PartialOrd, Eq, Ord)]
pub enum PinType {
    Input,
    Output,
    Bidirectional,
    Tristate,
    Passive,
    OpenCollector,
    OpenEmitter,
    NonConnected,
    PowerInput,
    PowerOutput,
    Unspecified,
}

impl PathStyle {
    pub fn new(stroke: Option<Color>,
               fill: Option<Color>,
               joins: Join,
               weight: u8,
               closed: bool
               ) -> Self {
        Self {stroke, fill, joins, weight, closed}
    }
    /// Returns a primary-color stroke style
    pub fn primary_default() -> Self {
        PathStyle {
            stroke: Some(Color::Primary),
            fill: None,
            joins: Join::Miter,
            weight: 2,
            closed: false,
        }
    }
}

impl TextStyle {
    pub fn default() -> Self {
        Self {
            stroke: Color::Primary,
            italic: false,
            bold: false,
            fontsize: 32,
            fontname: None,
            overline: false,
            jv: JustifyUD::Center,
            jh: JustifyRL::Center,
        }
    }
    /// Returns a text style that will position pin text correctly
    /// relative to the tip of the pin graphic
    pub fn terminal_default(orient: &PinOrientation, overline: bool) -> Self {
        let (v, h) = match orient {
            PinOrientation::Right => (JustifyUD::Above, JustifyRL::Left),
            PinOrientation::Left  => (JustifyUD::Above, JustifyRL::Right),
            PinOrientation::Down  => (JustifyUD::Above, JustifyRL::Right),
            PinOrientation::Up    => (JustifyUD::Below, JustifyRL::Right),
        };
        Self {
            stroke: Color::Primary,
            italic: false,
            bold: false,
            fontsize: 32,
            fontname: None,
            overline,
            jv: v,
            jh: h,
        }
    }
    pub fn bold(&self) -> bool {self.bold}
    pub fn italic(&self) -> bool {self.italic}
    pub fn overline(&self) -> bool {self.overline}
    pub fn vertical(&self) -> JustifyUD {self.jv}
    pub fn horizontal(&self) -> JustifyRL {self.jh}
    pub fn size(&self) -> f64 {
        self.fontsize as f64
    }
}

#[derive(Copy, Clone, Debug, Deserialize, Serialize, PartialEq)]
/// Options for justifing text on the axis of reading
pub enum JustifyRL {
    Left,
    Right,
    Center,
}

#[derive(Copy, Clone, Debug, Deserialize, Serialize, PartialEq)]
/// Options for justifing text on the orthogonal axis to reading
pub enum JustifyUD {
    Above,
    Below,
    Center,
}

impl Symbol {
    pub fn new(terminals: Vec<Terminal>,
               drawing: Vec<Graphic>,
               name: String) -> Self {
        let prefix = match name.chars().next() {
            // the ToUppercase iterator guarantees at least one uppercase
            Some(c) => c.to_uppercase().next().unwrap(),
            None => 'U'
        };
            Self {
                terminals, drawing, name,
                default_value: None,
                prefix
            }
    }
    pub fn new_sim(terminals: Vec<Terminal>,
               drawing: Vec<Graphic>,
               name: String,
               value: f64,
               prefix: char) -> Self {
            Self {
                terminals, drawing, name,
                default_value: Some(value),
                prefix,
            }
    }
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn terminals(&self) -> impl Iterator<Item=&Terminal> {
        self.terminals.iter()
    }
    pub fn drawing(&self) -> impl Iterator<Item=&Graphic> {
        self.drawing.iter()
    }
    pub fn default_value(&self) -> Option<f64> {
        self.default_value
    }
    /// Ensures all fields, names, etc., etc., do not contain
    /// control characters or other unwanted characters
    ///
    /// If such a field, etc., is found, it is returned. Otherwise,
    /// if all is well, None is returned.
    pub fn validate_strings(&self) -> Option<String> {
        if self.prefix.is_control() {
            return Some(format!("{}", self.prefix));
        }
        for elt in &self.drawing {
            if let Graphic::Text{content, ..} = elt {
                if let TextEntry::Custom(s) = content {
                    if let Some(s) = validate_string(s) {
                        return Some(s);
                    }
                }
            }
        }
        for term in &self.terminals {
            let x = validate_string(&term.name);
            if x.is_some() {
                return x;
            }
        }
        None
    }
    pub fn validate_term_indicies(&self) -> Option<u64> {
        let max_terms = self.terminals.len() as u64;
        for elt in &self.drawing {
            if let Graphic::Text{content, ..} = elt {
                if let TextEntry::Terminal(x) = content {
                    if *x >= max_terms {
                        return Some(*x);
                    }
                }
            }
        }
        None
    }
}
