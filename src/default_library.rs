//! Built-in Symbol Library
//!
//! As a stopgap until I get around to making a "full" component library, here are
//! some passives I've hacked together.

use crate::symbol::*;
use crate::symbol::PathElement::*;
use crate::legacy_symbol::PinOrientation;
use nalgebra::Vector2 as V2;

const GRID: i64 = 2500000;

/// Retrieve the baseline symbol library.
pub fn library() -> Vec<Symbol>{
    let mut out = vec![];
    out.push(resistor());
    out.push(capacitor());
    out.push(inductor());
    out.push(vsource());
    out
}

fn resistor() -> Symbol {
    let mut graphics = vec![];
    let mut terminals = vec![];
    // let's draw a little box
    let b = vec![
        Move(V2::new(-2*GRID, -2*GRID/3)),
        Line(V2::new( 2*GRID, -2*GRID/3)),
        Line(V2::new( 2*GRID,  2*GRID/3)),
        Line(V2::new(-2*GRID,  2*GRID/3)),
        Line(V2::new(-2*GRID, -2*GRID/3)),
    ];
    graphics.push(Graphic::Path(b, closed_style()));
    graphics.push(Graphic::Path(
            vec![Move(V2::new(-2*GRID, 0)), Line(V2::new(-3*GRID, 0))],
            PathStyle::primary_default()
            ));
    graphics.push(Graphic::Path(
            vec![Move(V2::new(2*GRID, 0)), Line(V2::new(3*GRID, 0))],
            PathStyle::primary_default()
            ));
    graphics.push(Graphic::Text {
        content: TextEntry::Reference,
        style: TextStyle::default(),
        loc: V2::new(0, GRID),
    });
    graphics.push(Graphic::Text {
        content: TextEntry::Value,
        style: TextStyle::default(),
        loc: V2::new(0, 0),
    });
    terminals.push(
        Terminal::new("".to_string(), PinType::Passive, V2::new(-3*GRID, 0), 0)
        );
    terminals.push(
        Terminal::new("".to_string(), PinType::Passive, V2::new( 3*GRID, 0), 1)
        );
    // default value: 1kOhm
    Symbol::new_sim(terminals, graphics, "R".to_string(), 1.0e3, 'R')
}

fn vsource() -> Symbol {
    let mut graphics = vec![];
    let mut terminals = vec![];
    graphics.push(Graphic::Path(
            vec![Arc{
                center: V2::new(0, 0), 
                radius: GRID as u64, 
                start: 0, 
                end: 1_000_000
            }],
            closed_style()
            ));
    graphics.push(Graphic::Path(
            vec![Move(V2::new(-GRID, 0)), Line(V2::new(-3*GRID, 0))],
            PathStyle::primary_default()
            ));
    graphics.push(Graphic::Path(
            vec![Move(V2::new(GRID, 0)), Line(V2::new(3*GRID, 0))],
            PathStyle::primary_default()
            ));
    graphics.push(Graphic::Text {
        content: TextEntry::Terminal(0),
        style: TextStyle::terminal_default(&PinOrientation::Right, false),
        loc: V2::new(-3*GRID, 0),
    });
    graphics.push(Graphic::Text {
        content: TextEntry::Terminal(1),
        style: TextStyle::terminal_default(&PinOrientation::Left, false),
        loc: V2::new( 3*GRID, 0),
    });
    graphics.push(Graphic::Text {
        content: TextEntry::Reference,
        style: TextStyle::default(),
        loc: V2::new(0, 3*GRID/2),
    });
    graphics.push(Graphic::Text {
        content: TextEntry::Value,
        style: TextStyle::default(),
        loc: V2::new(0, 0),
    });
    terminals.push(
        Terminal::new("-".to_string(), PinType::Passive, V2::new( 3*GRID, 0), 1)
        );
    terminals.push(
        Terminal::new("+".to_string(), PinType::Passive, V2::new(-3*GRID, 0), 0)
        );
    // default value: 1V
    Symbol::new_sim(terminals, graphics, "V".to_string(), 1.0, 'V')
}

fn capacitor() -> Symbol {
    let mut graphics = vec![];
    let mut terminals = vec![];
    graphics.push(Graphic::Path(
            vec![Move(V2::new(-GRID/4, -GRID)), Line(V2::new(-GRID/4, GRID))],
            PathStyle::new(Some(Color::Primary), None, Join::Round, 10, false)
            ));
    graphics.push(Graphic::Path(
            vec![Move(V2::new(GRID/4, -GRID)), Line(V2::new(GRID/4, GRID))],
            PathStyle::new(Some(Color::Primary), None, Join::Round, 10, false)
            ));
    graphics.push(Graphic::Path(
            vec![Move(V2::new(-GRID/4, 0)), Line(V2::new(-3*GRID, 0))],
            PathStyle::primary_default()
            ));
    graphics.push(Graphic::Path(
            vec![Move(V2::new(GRID/4, 0)), Line(V2::new(3*GRID, 0))],
            PathStyle::primary_default()
            ));
    graphics.push(Graphic::Text {
        content: TextEntry::Terminal(0),
        style: TextStyle::terminal_default(&PinOrientation::Right, false),
        loc: V2::new(-3*GRID, 0),
    });
    graphics.push(Graphic::Text {
        content: TextEntry::Terminal(1),
        style: TextStyle::terminal_default(&PinOrientation::Left, false),
        loc: V2::new( 3*GRID, 0),
    });
    graphics.push(Graphic::Text {
        content: TextEntry::Reference,
        style: TextStyle::default(),
        loc: V2::new(0, -3*GRID/2),
    });
    graphics.push(Graphic::Text {
        content: TextEntry::Value,
        style: TextStyle::default(),
        loc: V2::new(0, 3*GRID/2),
    });
    terminals.push(
        Terminal::new("".to_string(), PinType::Passive, V2::new(-3*GRID, 0), 0)
        );
    terminals.push(
        Terminal::new("".to_string(), PinType::Passive, V2::new( 3*GRID, 0), 1)
        );
    // default value: 1uF
    Symbol::new_sim(terminals, graphics, "C".to_string(), 1e-6, 'C')
}

// TODO TEMPORARY -- until I get a bezier type implemented, this one's ugly.
fn inductor() -> Symbol {
    let mut graphics = vec![];
    let mut terminals = vec![];
    graphics.push(Graphic::Path(
            vec![
            Move(V2::new(3*GRID, 0)),
            Line(V2::new(5*GRID/4, 0)),
            Arc{center: V2::new(GRID, 0), radius: GRID as u64/4, 
                start: 0, end: 500_000},
            Arc{center: V2::new(GRID/2, 0), radius: GRID as u64/4, start: 0, end: 500_000},
            Arc{center: V2::new(0, 0), radius: GRID as u64/4, start: 0, end: 500_000},
            Arc{center: V2::new(-GRID/2, 0), radius: GRID as u64/4, start: 0, end: 500_000},
            Arc{center: V2::new(-GRID, 0), radius: GRID as u64/4, start: 0, end: 500_000},
            Line(V2::new(-3*GRID, 0))
            ],
            PathStyle::primary_default()
            ));
    graphics.push(Graphic::Text {
        content: TextEntry::Terminal(0),
        style: TextStyle::terminal_default(&PinOrientation::Right, false),
        loc: V2::new(-3*GRID, 0),
    });
    graphics.push(Graphic::Text {
        content: TextEntry::Terminal(1),
        style: TextStyle::terminal_default(&PinOrientation::Left, false),
        loc: V2::new( 3*GRID, 0),
    });
    graphics.push(Graphic::Text {
        content: TextEntry::Reference,
        style: TextStyle::default(),
        loc: V2::new(0, GRID),
    });
    graphics.push(Graphic::Text {
        content: TextEntry::Value,
        style: TextStyle::default(),
        loc: V2::new(0, -GRID/2),
    });
    terminals.push(
        Terminal::new("".to_string(), PinType::Passive, V2::new(-3*GRID, 0), 0)
        );
    terminals.push(
        Terminal::new("".to_string(), PinType::Passive, V2::new( 3*GRID, 0), 1)
        );
    // default value: 10uH
    Symbol::new_sim(terminals, graphics, "L".to_string(), 10e-6, 'L')
}

fn closed_style() -> PathStyle {
    PathStyle::new(Some(Color::Primary), None, Join::Miter, 4, true)
}
