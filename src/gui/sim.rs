use std::sync::mpsc::{Sender, Receiver};
use std::{sync::mpsc, thread};

use crate::model::Sheet;
use crate::model::HandleType;
use crate::gui::queue_redraw_sim_plot;
use crate::{UserMessage, post_to_user};

use splinulator as sim;

use glib::idle_add;

use plotters::prelude::*;
use plotters::style::Palette99;
use plotters_cairo::CairoBackend;

use nalgebra as na;


// TODO: replace with user parameter or actual calculation
const MAX_TIME: f64 = 3e-3;

/// Result of a simulation run.
type SimResult = Result<sim::Series<f64, na::Dynamic>, sim::Error>;

/// Message passing and shared state with the simulation thread
///
/// Because splinulation^H^H^H^H simulation can be slow, we don't want
/// to block the UI thread. So, here, it's been sent off to run concurrently,
/// and it'll ping us back when it's done. 
pub enum SimulationThread {
    /// Not spawned, no communication buffers
    Uninitialized,
    /// Spawned, we can send it messages and it talks back
    Spawned(ThreadHandle),
}

pub struct ThreadHandle {
    _join: thread::JoinHandle<()>,
    outgoing: Sender<sim::System>,
    incoming: Receiver<(u64, SimResult)>,
    latest: Option<(u64, sim::Series<f64, na::Dynamic>)>,
}


impl SimulationThread {
    fn ensure_init(&mut self) -> 
        (&mut mpsc::Sender<sim::System>,
         &mut mpsc::Receiver<(u64, SimResult)>, 
         &mut Option<(u64, sim::Series<f64, na::Dynamic>)>) {
            if let Self::Spawned(t) = self {
                return (&mut t.outgoing, &mut t.incoming, &mut t.latest);
            }
            // Spin up a new thread
            let (tx, rx) = mpsc::channel();
            let (otx, orx) = mpsc::channel();
            let join = thread::spawn(move || {
                sim_thread(rx, otx)
            });
            *self = Self::Spawned(
                ThreadHandle {
                    _join: join,
                    outgoing: tx,
                    incoming: orx,
                    latest: None,
                });
            self.ensure_init()
    }
    pub fn queue_simulation(&mut self, sheet: &Sheet) {
        let tx = self.ensure_init().0;
        let cmpts: Vec<sim::Component> = sheet.symbols()
            .filter(|s| s.value().is_some())
            .map(|s| {
            // this is an objectively stupid way to do this
            // TODO: actually add a model parameter to things
            match s.symbol().name() {
                "R" => sim::Component::Resistor(s.value().unwrap()),
                "L" => sim::Component::Inductor(s.value().unwrap()),
                "C" => sim::Component::Capacitor(s.value().unwrap()),
                "V" => sim::Component::VSource(s.value().unwrap()),
                _ => unreachable!()// ignore non-sim components
            }
        }).collect();
        let mut component_handles = vec![];
        for node in sheet.nodes() {
            let mut node_handles = vec![];
            let handles = node.handles();
            for handle in handles {
                if let HandleType::Terminal{parent_index, terminal_enid, ..}
                    = handle.typ() {
                        node_handles.push(sim::ComponentHandle {
                            term: *terminal_enid as usize,
                            comp: *parent_index,
                        });
                }
            }
            component_handles.push(node_handles);
        }
        let sys = match sim::System::new(cmpts, component_handles, 0) {
            Ok(s) => s,
            Err(e) => { 
                post_to_user(UserMessage::SimFailure(e.to_string()));
                return;
            }
        };
        tx.send(sys).unwrap();
    }
    pub fn render_latest(&mut self, ctx: CairoBackend<'_>) {
        let latest = match self.get_latest() {
            Some(s) => s,
            None => return,
        };
        let root = ctx.into_drawing_area();
        root.fill(&WHITE).unwrap();
        let mut plot = ChartBuilder::on(&root)
            .caption("Simulation output", 
                     ("sans-serif", 50).into_font())
            .margin(5)
            .x_label_area_size(30)
            .y_label_area_size(30)
            .build_cartesian_2d(0f32..(MAX_TIME as f32),-1f32..1f32)
            .expect("Failed to build plot!");
        plot.configure_mesh().draw().expect("Failed to draw mesh");
        let arr: Vec<sim::CircuitQuantity> = 
            latest.base_quantities()
            .map(|x| *x)
            .collect();
        for (i, elt) in arr.iter().enumerate() {
            // plot a line series for each
            let arr_i = [*elt];
            let label = format!("{:?}", elt);
            plot.draw_series(LineSeries::new(
                    latest.iter(&arr_i[..]).unwrap().map(
                        |x| (x.indep() as f32, x.get(0) as f32)
                    ), Palette99::pick(i)))
                .expect("Failed to plot series?")
                .label(&label)
                .legend(move |(x, y)| PathElement::new(vec![(x, y), (x+20, y)], 
                                                  Palette99::pick(i)));
        }
        plot
            .configure_series_labels()
            .background_style(&WHITE.mix(0.8))
            .border_style(&BLACK)
            .draw()
            .expect("Failed to draw plot");
        root.present().expect("Failed to present plot");
    }
    fn get_latest(&mut self) -> Option<&sim::Series<f64, na::Dynamic>> {
        let (_, rx, latest) = self.ensure_init();
        while let Ok(d) = rx.try_recv() {
            if let Some(old) = latest {
                if let (i, Ok(q)) = d {
                    if i > old.0 {
                        *latest = Some((i, q));
                    }
                }
            } else if let (i, Ok(q)) = d {
                *latest = Some((i, q));
            } else if let (_, Err(e)) = d {
                    post_to_user(UserMessage::SimFailure(e.to_string()));
            }
        }
        match latest {
            Some(x) => Some(&x.1),
            None => None
        }
    }
}

fn sim_thread(rx: Receiver<sim::System>, tx: Sender<(u64, SimResult)>) -> ! {
    let mut i = 0;
    loop {
        let sys = rx.recv().unwrap();
        // TODO: get sim params from user (or maybe calculate from system
        // eigenvalues or use a smart simulation method)?
        let res = sys.transient_fts(1e-5, MAX_TIME);
        tx.send((i, res)).unwrap();
        i += 1;
        idle_add(move || {
            queue_redraw_sim_plot();
            glib::Continue(false)
        });
    }
}
