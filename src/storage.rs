//! Implements internal file format saving and retrieval
//!
//! Includes a rudimentary versioning system.

use crate::model::{FrozenSheet, Sheet, ThawError};

use std::{fmt, fs, io};
use std::fmt::Display;
use std::io::BufReader;
use std::path::PathBuf;

use serde_json;
use bincode;
use serde::{Serialize, Deserialize};

use chrono::{Utc, DateTime};

use bincode::Options;

use systemstat::Platform;

// Lowest reasonable filesize limit
const DEFAULT_FILESIZE_LIMIT: u64 = 50_000_000;

#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
enum VersionedFile {
    Alpha(AlphaFormat),
}

#[derive(Serialize, Deserialize, Debug)]
struct AlphaFormat {
    modified: DateTime<Utc>,
    sheets: Vec<FrozenSheet>,
}

#[derive(Debug)]
pub enum StorageError {
    Json(serde_json::Error),
    Bincode(bincode::Error),
    Fs(io::Error),
    Thaw(ThawError),
} 

pub fn save_json(to: PathBuf, sheets: Vec<&Sheet>) -> Result<(),StorageError> {
    let mut frozen = vec![];
    for sheet in sheets {
        frozen.push(FrozenSheet::freeze(sheet));
    }
    let format = AlphaFormat {
        modified: Utc::now(),
        sheets: frozen,
    };
    let data = VersionedFile::Alpha(format);
    let file = fs::File::create(to)?;
    serde_json::to_writer(file, &data)?;
    Ok(())
}

pub fn load_json(from: PathBuf) -> Result<Vec<Sheet>, StorageError> {
    let file = fs::File::open(from)?;
    let buf = BufReader::new(file);
    let frozen: VersionedFile = serde_json::de::from_reader(buf)?;
    let VersionedFile::Alpha(a) = frozen;
    eprintln!("Last modified: {:?}", a.modified);
    let mut sheets = vec![];
    for sheet in a.sheets {
        sheets.push(sheet.thaw()?);
    }
    Ok(sheets)
}

pub fn save_binary_file(to: PathBuf, sheets: Vec<&Sheet>) -> Result<(),StorageError> {
    let mut frozen = vec![];
    for sheet in sheets {
        frozen.push(FrozenSheet::freeze(sheet));
    }
    let format = AlphaFormat {
        modified: Utc::now(),
        sheets: frozen,
    };
    let data = VersionedFile::Alpha(format);
    let file = fs::File::create(to)?;
    let mem_limit = match systemstat::System.memory() {
        Ok(systemstat::data::Memory {total, ..}) => {
            // one-fifth system memory or 50 megs, whichever bigger
            u64::max(DEFAULT_FILESIZE_LIMIT, total.0/5)
        },
        Err(_) => {
            // reasonable default: 50 megs
            DEFAULT_FILESIZE_LIMIT
        }
    };
    let bcfg = bincode::options().
        with_limit(mem_limit);
    //bincode::serialize_into(file, &data)?;
    bcfg.serialize_into(file, &data)?;
    Ok(())
}

pub fn load_binary_file(from: PathBuf) -> Result<Vec<Sheet>, StorageError> {
    let file = fs::File::open(from)?;
    let buf = BufReader::new(file);
    load_binary_from(buf)
}

pub fn load_binary_from<R: io::Read>(from: BufReader<R>) -> Result<Vec<Sheet>, StorageError> {
    let mem_limit = match systemstat::System.memory() {
        Ok(systemstat::data::Memory {total, ..}) => {
            // one-fifth system memory or 50 megs, whichever bigger
            u64::max(DEFAULT_FILESIZE_LIMIT, total.0/5)
        },
        Err(_) => {
            // reasonable default: 50 megs
            DEFAULT_FILESIZE_LIMIT
        }
    };
    let bcfg = bincode::options().
        with_limit(mem_limit);
    println!("Calculated mem limit: {}", mem_limit);
    let versioned: VersionedFile = bcfg.deserialize_from(from)?;
    let VersionedFile::Alpha(a) = versioned;
    eprintln!("Last modified: {:?}", a.modified);
    let mut sheets = vec![];
    for sheet in a.sheets {
        sheets.push(sheet.thaw()?);
    }
    Ok(sheets)
}

impl Display for StorageError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Json(x) => x.fmt(f),
            Self::Bincode(x) => x.fmt(f),
            Self::Thaw(x) => x.fmt(f),
            Self::Fs(x) => x.fmt(f),
        }
    }
}

impl From<io::Error> for StorageError {
    fn from(e: io::Error) -> Self {
        Self::Fs(e)
    }
}

impl From<serde_json::Error> for StorageError {
    fn from(e: serde_json::Error) -> Self {
        Self::Json(e)
    }
}

impl From<bincode::Error> for StorageError {
    fn from(e: bincode::Error) -> Self {
        Self::Bincode(e)
    }
}

impl From<ThawError> for StorageError {
    fn from(e: ThawError) -> Self {
        Self::Thaw(e)
    }
}

pub fn validate_string(s: &str) -> Option<String> {
    for c in s.chars() {
        if c.is_control() {
            // No control characters in symbol names!
            return Some(s.to_string());
        }
    }
    None
}
