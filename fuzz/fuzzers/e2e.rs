use multyschema::storage::load_binary_from;
use multyschema::view::{Viewport, redraw_schematic};
use cairo::{PdfSurface, Context};
use afl::fuzz;
use std::io::BufReader;

fn main() {
    // The test: load a file, parse it, and then render it to
    // a PDF without crashing. The PDF file will be printed to STDOUT.
    fuzz!(|data: &[u8]| {
        // Does it load and at worst fail gracefully?
        let data = BufReader::new(data);
        match load_binary_from(data) {
            Ok(mut sheets) => {
                if let Some(sheet) = sheets.pop() {
                    // and does it render?
                    let dest = std::io::stdout();
                    let surface = PdfSurface::for_stream(1920.0, 1080.0, dest).unwrap();
                    let ctx = Context::new(&surface).unwrap();
                    let v = Viewport::new();
                    redraw_schematic(&v, &ctx, &sheet);
                    surface.finish();
                }
            },
            Err(_) => {}
        }
    });
}
