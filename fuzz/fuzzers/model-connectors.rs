use multyschema::model::{FlopDirection, Sheet};
use afl::fuzz;

fn main() {
    let grid = 1250000;
    fuzz!(|data: &[u8]| {
        assert_eq!(data.len(), 30*3);
        let mut s = Sheet::empty_default();
        for data in data.chunks(5) {
            let (a, b) = (
                data[0] as i64 -128,
                data[1] as i64 -128,
                );
            let (x, y, z, w) = (a%16, a/16, b%16, b/16);
            let flop = if data[3]%2 == 1 {
                FlopDirection::XThenY
            } else {
                FlopDirection::YThenX
            };
            s.insert_wire((x*grid, y*grid), (z*grid, w*grid), flop);
        }
    });
}
