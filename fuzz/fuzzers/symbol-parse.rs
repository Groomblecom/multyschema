use multyschema::symbol::parse_library;
use afl::fuzz;
use std::str::from_utf8;

fn main() {
    fuzz!(|data: &[u8]| {
        let libstring = from_utf8(data).unwrap();
        let _ = parse_library(libstring);
    });
}
